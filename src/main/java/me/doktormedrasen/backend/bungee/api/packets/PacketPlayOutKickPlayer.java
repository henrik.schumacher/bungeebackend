package me.doktormedrasen.backend.bungee.api.packets;

import lombok.AllArgsConstructor;
import me.doktormedrasen.backend.telnet.OutgoingPacket;
import me.doktormedrasen.backend.telnet.PacketDataSerializer;

import java.util.UUID;

/**
 * Class by DoktorMedRasen :P
 */

@AllArgsConstructor
public class PacketPlayOutKickPlayer implements OutgoingPacket {

    private UUID uuid;
    private String reason;

    public void encode(PacketDataSerializer serializer) {
        serializer.writeUUID(this.uuid);
        serializer.writeString(this.reason);
    }

}
