package me.doktormedrasen.backend.bungee.api.packets;

import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import me.doktormedrasen.backend.bungee.api.manager.BackendManager;
import me.doktormedrasen.backend.telnet.IncomingPacket;
import me.doktormedrasen.backend.telnet.PacketDataSerializer;
import me.doktormedrasen.backend.telnet.TelnetHandler;

import java.util.UUID;

/**
 * Class by DoktorMedRasen :P
 */

public class PacketPlayInPlayerQuit implements IncomingPacket {

    private UUID uuid;

    public void decode(PacketDataSerializer serializer) {
        this.uuid = serializer.readUUID();
    }

    public void handle(TelnetHandler handler) {
        BackendPlayer player = BackendManager.getInstance().getPlayer(this.uuid);
        if (player != null) {
            BackendManager.getInstance().removePlayer(player);
        }
    }

}
