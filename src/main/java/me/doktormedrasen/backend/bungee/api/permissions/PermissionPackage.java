package me.doktormedrasen.backend.bungee.api.permissions;

import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.model.Updates;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import me.doktormedrasen.backend.bungee.api.manager.MongoManager;
import me.doktormedrasen.backend.bungee.api.manager.PermissionManager;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Class by DoktorMedRasen :P
 */

@Getter
@Setter
@AllArgsConstructor
public class PermissionPackage {

    private String name;
    private List<String> permissions;

    public PermissionPackage(Document document) {
        if (document == null) return;
        this.name = document.getString("name");
        this.permissions = document.get("permissions", new ArrayList<>());
    }

    public static PermissionPackage load(String name) {
        return fromDocument(MongoManager.getInstance().getPackagesCollection().find(Filters.eq("name", name.toLowerCase())).first());
    }

    private static PermissionPackage fromDocument(Document document) {
        if (document == null) return null;

        return new PermissionPackage(
                document.getString("name"),
                document.get("permissions", new ArrayList<>())
        );
    }

    public void save() {
        MongoManager.getInstance().getPackagesCollection().updateOne(
                Filters.eq("name", this.name),
                Updates.combine(
                        Updates.set("permissions", this.permissions)
                ),
                new UpdateOptions().upsert(true)
        );
    }

    public void reload() {
        Document document = PermissionManager.getInstance().loadPackage(this.name);
        if (document == null) return;
        this.permissions = document.get("permissions", new ArrayList<>());
    }

    public void delete() {
        MongoManager.getInstance().getPackagesCollection().deleteOne(Filters.eq("name", this.name));
    }

}
