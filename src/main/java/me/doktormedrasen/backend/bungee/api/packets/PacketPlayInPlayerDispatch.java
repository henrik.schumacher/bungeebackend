package me.doktormedrasen.backend.bungee.api.packets;

import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import me.doktormedrasen.backend.bungee.api.manager.BackendManager;
import me.doktormedrasen.backend.telnet.IncomingPacket;
import me.doktormedrasen.backend.telnet.PacketDataSerializer;
import me.doktormedrasen.backend.telnet.TelnetHandler;
import net.md_5.bungee.api.ProxyServer;

import java.util.UUID;

/**
 * Class by DoktorMedRasen :P
 */

public class PacketPlayInPlayerDispatch implements IncomingPacket {

    private UUID uuid;
    private String data;

    public void decode(PacketDataSerializer serializer) {
        this.uuid = serializer.readUUID();
        this.data = serializer.readString();
    }

    public void handle(TelnetHandler handler) {
        BackendPlayer player = BackendManager.getInstance().getPlayer(this.uuid);
        if (player.isLocal()) {
            if (this.data.startsWith("/")) {
                ProxyServer.getInstance().getPluginManager().dispatchCommand(player.getConnection(), this.data.substring(1));
            } else {
                player.getConnection().chat(this.data);
            }
        }
    }

}
