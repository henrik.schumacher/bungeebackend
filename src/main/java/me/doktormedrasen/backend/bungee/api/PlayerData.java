package me.doktormedrasen.backend.bungee.api;

import lombok.Getter;
import me.doktormedrasen.backend.bungee.api.manager.*;
import me.doktormedrasen.backend.bungee.api.permissions.PermissionGroup;
import me.doktormedrasen.backend.bungee.api.permissions.PermissionPackage;
import me.doktormedrasen.backend.bungee.api.punishment.BackendBan;
import me.doktormedrasen.backend.bungee.api.punishment.BackendMute;
import org.bson.Document;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Class by DoktorMedRasen :P
 */

@Getter
public class PlayerData {

    private Document document;
    private UUID uniqueId;
    private String name;
    private BackendBan ban;
    private BackendMute mute;
    private PermissionGroup group;
    private List<PermissionGroup> inheritances;
    private List<PermissionPackage> packages;
    private List<String> permissions;
    private BackendLanguage language;
    private String displayName;
    private Date firtseen;
    private Date lastseen;

    public PlayerData(Document document) {
        this.document = document;
        this.uniqueId = UUID.fromString(document.getString("uuid"));
        this.name = document.getString("name");
        this.ban = PunishmentManager.getInstance().parseBan(document);
        this.mute = PunishmentManager.getInstance().parseMute(document);
        this.group = PermissionManager.getInstance().parseGroup(document);
        this.inheritances = PermissionManager.getInstance().parseInheritances(document);
        this.packages = PermissionManager.getInstance().parsePackages(document);
        this.permissions = PermissionManager.getInstance().parsePermissions(document);
        this.language = LanguageManager.getInstance().parseLanguage(document);
        this.displayName = this.group.getChatPrefix() + this.name;
        this.firtseen = LoginManager.getInstance().parseFirstSeen(document);
        this.lastseen = LoginManager.getInstance().parseLastSeen(document);
    }

    public void reloadPunishment() {
        this.ban = PunishmentManager.getInstance().parseBan(this.document);
        this.mute = PunishmentManager.getInstance().parseMute(this.document);
    }

    public void reloadPermissions() {
        this.group = PermissionManager.getInstance().parseGroup(this.document);
        this.inheritances = PermissionManager.getInstance().parseInheritances(this.document);
        this.packages = PermissionManager.getInstance().parsePackages(this.document);
        this.permissions = PermissionManager.getInstance().parsePermissions(this.document);
        this.displayName = this.group.getChatPrefix() + this.name;
    }

    public void reloadLanguage() {
        this.language = LanguageManager.getInstance().parseLanguage(this.document);
    }

    public void setGroup(PermissionGroup group) {
        this.group = group;
        PermissionManager.getInstance().saveAndUpdateGroup(this.uniqueId, group);
    }

    public void setInheritances(List<PermissionGroup> inheritances) {
        this.inheritances = inheritances;
        PermissionManager.getInstance().saveAndUpdateInheritances(this.uniqueId, inheritances);
    }

    public void setPackages(List<PermissionPackage> packages) {
        this.packages = packages;
        PermissionManager.getInstance().saveAndUpdatePackages(this.uniqueId, packages);
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
        PermissionManager.getInstance().saveAndUpdatePermissions(this.uniqueId, permissions);
    }

    public void setLanguage(BackendLanguage language) {
        this.language = language;
        LanguageManager.getInstance().saveAndUpdateLanguage(this.uniqueId, language);
    }

    public boolean isBanned() {
        if (this.ban != null && this.ban.getUntil().getTime() != -1 && this.ban.getUntil().getTime() < System.currentTimeMillis()) {
            PunishmentManager.getInstance().logPunishmentRemoval(this.uniqueId, this.ban, BackendManager.getInstance().getConsole(), "expired");
            this.setBan(null);
        }
        return this.ban != null;
    }

    public boolean isMuted() {
        if (this.mute != null && this.mute.getUntil().getTime() != -1L && this.mute.getUntil().getTime() < System.currentTimeMillis()) {
            PunishmentManager.getInstance().logPunishmentRemoval(this.uniqueId, this.mute, BackendManager.getInstance().getConsole(), "expired");
            this.setMute(null);
        }
        return this.mute != null;
    }

    public void setBan(BackendBan ban) {
        this.ban = ban;
        PunishmentManager.getInstance().saveAndUpdateBan(this.uniqueId, ban);
    }

    public void setMute(BackendMute mute) {
        this.mute = mute;
        PunishmentManager.getInstance().saveAndUpdateMute(this.uniqueId, mute);
    }

    public String language(String german, String english) {
        return this.language.equals(BackendLanguage.ENGLISH) ? english : german;
    }

    public boolean hasPermission(String permission) {
        permission = permission.toLowerCase();
        boolean bool = false;
        if (this.permissions.contains("*")) {
            bool = true;
        }
        if (this.group.superContainsPermission("*")) {
            bool = true;
        }
        for (final PermissionGroup group : this.inheritances) {
            if (group.superContainsPermission("*")) {
                bool = true;
            }
        }
        for (final PermissionPackage permissionPackage : this.packages) {
            if (permissionPackage.getPermissions().contains("*")) {
                bool = true;
            }
        }
        final String[] array = permission.split("\\.");
        for (int i = 0; i < array.length - 1; ++i) {
            final StringBuilder builder = new StringBuilder();
            for (int o = 0; o <= i; ++o) {
                builder.append(array[o]).append(".");
            }
            builder.append("*");
            for (final PermissionPackage permissionPackage2 : this.packages) {
                if (permissionPackage2.getPermissions().contains(builder.toString())) {
                    bool = true;
                }
                if (permissionPackage2.getPermissions().contains("-" + builder.toString())) {
                    bool = false;
                }
            }
            for (final PermissionGroup group2 : this.inheritances) {
                if (group2.superContainsPermission(builder.toString())) {
                    bool = true;
                }
                if (group2.superContainsPermission("-" + builder.toString())) {
                    bool = false;
                }
            }
            if (this.group.superContainsPermission(builder.toString())) {
                bool = true;
            }
            if (this.group.superContainsPermission("-" + builder.toString())) {
                bool = false;
            }
            if (this.permissions.contains(builder.toString())) {
                bool = true;
            }
            if (this.permissions.contains("-" + builder.toString())) {
                bool = false;
            }
        }
        for (final PermissionPackage permissionPackage3 : this.packages) {
            if (permissionPackage3.getPermissions().contains(permission)) {
                bool = true;
            }
            if (permissionPackage3.getPermissions().contains("-" + permission)) {
                bool = false;
            }
        }
        for (final PermissionGroup group3 : this.inheritances) {
            if (group3.superContainsPermission(permission)) {
                bool = true;
            }
            if (group3.superContainsPermission("-" + permission)) {
                bool = false;
            }
        }
        if (this.group.superContainsPermission(permission)) {
            bool = true;
        }
        if (this.group.superContainsPermission("-" + permission)) {
            bool = false;
        }
        if (this.permissions.contains(permission)) {
            bool = true;
        }
        if (this.permissions.contains("-" + permission)) {
            bool = false;
        }
        return bool;
    }

}
