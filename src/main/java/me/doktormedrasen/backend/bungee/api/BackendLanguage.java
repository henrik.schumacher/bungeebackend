package me.doktormedrasen.backend.bungee.api;

public enum BackendLanguage {

    GERMAN,
    ENGLISH

}
