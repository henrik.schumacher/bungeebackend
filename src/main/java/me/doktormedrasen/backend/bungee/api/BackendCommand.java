package me.doktormedrasen.backend.bungee.api;

import me.doktormedrasen.backend.bungee.Backend;
import me.doktormedrasen.backend.bungee.api.manager.BackendManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

import java.util.Collection;
import java.util.Collections;

/**
 * Class by DoktorMedRasen :P
 */

public abstract class BackendCommand extends Command implements TabExecutor {

    public BackendCommand(String name) {
        super(name);
    }

    public void execute(CommandSender commandSender, String[] strings) {
        final BackendSender sender = BackendManager.getInstance().getBackendSender(commandSender);
        if (sender == null) {
            return;
        }
        try {
            this.execute(sender, strings);
        } catch (Exception e) {
            e.printStackTrace();
            sender.sendMessage(
                    Backend.PREFIX + "§cEs ist ein unerwarteter Fehler aufgetreten",
                    Backend.PREFIX + "An internal error occurred whilst executing this command"
            );
            if (sender.hasPermission("system.error")) {
                sender.sendMessage(
                        Backend.PREFIX + "§cFehler§8: §7" + e.getMessage(),
                        Backend.PREFIX + "§cError§8: §7" + e.getMessage()
                );
            }
            sender.sendMessage(
                    Backend.PREFIX + "§cBitte wende dich an ein Teammitglied",
                    Backend.PREFIX + "§cPlease contact a staff member"
            );
        }
    }

    public abstract void execute(BackendSender var1, String[] var2);

    public Iterable<String> onTabComplete(CommandSender commandSender, String[] strings) {
        final Iterable<String> matches = this.onTabComplete(BackendManager.getInstance().getBackendSender(commandSender), strings);
        if (matches == null) {
            return Collections.emptyList();
        }
        return matches;
    }

    public abstract Iterable<String> onTabComplete(BackendSender var1, String[] var2);

}
