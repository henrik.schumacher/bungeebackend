package me.doktormedrasen.backend.bungee.api.manager;

import com.mongodb.client.model.Filters;
import lombok.Getter;
import me.doktormedrasen.backend.bungee.Backend;
import me.doktormedrasen.backend.bungee.api.BackendLanguage;
import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import me.doktormedrasen.backend.bungee.api.packets.PacketPlayOutCustomMessage;
import me.doktormedrasen.backend.utils.ByteArrayDataInput;
import me.doktormedrasen.backend.utils.ByteArrayDataOutput;
import org.bson.Document;

import java.util.UUID;

/**
 * Class by DoktorMedRasen
 */

public class LanguageManager {

    @Getter
    private static LanguageManager instance;

    public LanguageManager() {
        instance = this;
        Backend.getInstance().enableModule(this);
    }

    public void disable() {
        instance = null;
        Backend.getInstance().disableModule(this);
    }

    public BackendLanguage parseLanguage(Document document) {
        if (document != null && document.containsKey("language")) {
            return BackendLanguage.valueOf(document.getString("language"));
        }
        return BackendLanguage.GERMAN;
    }

    public void saveAndUpdateLanguage(UUID uuid, BackendLanguage language) {
        MongoManager.getInstance().getPlayersCollection().updateOne(
          Filters.eq("uuid", uuid.toString()),
                new Document("$set", new Document("language", language.name()))
        );
        this.sendPlayerUpdate(uuid);
    }

    public void sendPlayerUpdate(UUID uuid) {
        ByteArrayDataOutput output = new ByteArrayDataOutput();
        output.writeString("update");
        output.writeUUID(uuid);
        PacketPlayOutCustomMessage packet = new PacketPlayOutCustomMessage(new ByteArrayDataOutput().writeByte((byte) 1).toByteArray(), "languagemanager", output.toByteArray());
        BackendManager.getInstance().sendPacket(packet);
        packet = new PacketPlayOutCustomMessage(new ByteArrayDataOutput().writeByte((byte) 6).writeUUID(uuid).toByteArray(), "languagemanager", output.toByteArray());
        BackendManager.getInstance().sendPacket(packet);
    }

    public void onRecieve(ByteArrayDataInput input) {
        String method = input.readString();
        if (method.equals("update")) {
            BackendPlayer player = BackendManager.getInstance().getPlayer(input.readUUID());
            player.reloadLanguage();
        }
    }

}
