package me.doktormedrasen.backend.bungee.api.manager;

import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import lombok.Getter;
import me.doktormedrasen.backend.bungee.Backend;
import me.doktormedrasen.backend.bungee.api.BackendConsole;
import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import me.doktormedrasen.backend.bungee.api.BackendSender;
import me.doktormedrasen.backend.bungee.api.events.telnet.TelnetConnectEvent;
import me.doktormedrasen.backend.bungee.api.events.telnet.TelnetDisconnectEvent;
import me.doktormedrasen.backend.bungee.api.packets.*;
import me.doktormedrasen.backend.telnet.OutgoingPacket;
import me.doktormedrasen.backend.telnet.TelnetClient;
import me.doktormedrasen.backend.telnet.TelnetHandler;
import me.doktormedrasen.backend.telnet.TelnetInstance;
import me.doktormedrasen.backend.utils.ByteArrayDataOutput;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import net.md_5.bungee.util.CaseInsensitiveMap;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Class by DoktorMedRasen :P
 */

@Getter
public class BackendManager {

    @Getter
    private static BackendManager instance;
    private TelnetClient client;
    private TelnetHandler handler;
    private ScheduledTask task;
    private ReadWriteLock loginLock;
    private Map<UUID, Document> logins;
    private ReadWriteLock connectionLock;
    private Map<UUID, BackendPlayer> connections;
    private Map<String, BackendPlayer> connectionsByName;

    public BackendManager() {
        instance = this;
        this.loginLock = new ReentrantReadWriteLock();
        this.logins = new WeakHashMap<>();
        this.connectionLock = new ReentrantReadWriteLock();
        this.connections = new HashMap<>();
        this.connectionsByName = new CaseInsensitiveMap<>();
        Backend.getInstance().enableModule(this);
    }

    public void disable() {
        ProxyServer.getInstance().getScheduler().cancel(this.task);
        try {
            this.client.disconnect();
        } catch (Exception e) {}
        instance = null;
        Backend.getInstance().disableModule(this);
    }

    public void connect() {
        this.client = new TelnetClient(Backend.getInstance().getConfiguration().getString("manager.host"), Backend.getInstance().getConfiguration().getInt("manager.port"));
        this.client.setListener(new TelnetInstance.TelnetListener() {
            @Override
            public void onConnect(TelnetHandler handler) {
                BackendManager.this.handler = handler;
                handler.registerIncomingPacket(1, PacketPlayInConnectToServer.class);
                handler.registerIncomingPacket(2, PacketPlayInCustomMessage.class);
                handler.registerIncomingPacket(3, PacketPlayInKickPlayer.class);
                handler.registerIncomingPacket(4, PacketPlayInPlayerDispatch.class);
                handler.registerIncomingPacket(5, PacketPlayInPlayerJoin.class);
                handler.registerIncomingPacket(6, PacketPlayInPlayerLatency.class);
                handler.registerIncomingPacket(7, PacketPlayInPlayerQuit.class);
                handler.registerIncomingPacket(8, PacketPlayInPlayerRequest.class);
                handler.registerIncomingPacket(9, PacketPlayInSendMessage.class);
                handler.registerIncomingPacket(10, PacketPlayInServerSwitch.class);
                handler.registerOutgoingPacket(1, PacketPlayOutConnectToServer.class);
                handler.registerOutgoingPacket(2, PacketPlayOutCustomMessage.class);
                handler.registerOutgoingPacket(3, PacketPlayOutHandshake.class);
                handler.registerOutgoingPacket(4, PacketPlayOutKickPlayer.class);
                handler.registerOutgoingPacket(5, PacketPlayOutPlayerJoin.class);
                handler.registerOutgoingPacket(6, PacketPlayOutPlayerQuit.class);
                handler.registerOutgoingPacket(7, PacketPlayOutPlayers.class);
                handler.registerOutgoingPacket(8, PacketPlayOutSendMessage.class);
                handler.registerOutgoingPacket(9, PacketPlayOutServerSwitch.class);
                handler.registerOutgoingPacket(10, PacketPlayOutSlots.class);
                handler.sendPacket(new PacketPlayOutHandshake("proxy", Backend.getInstance().getProxyName()));
                BackendManager.this.connectionLock.readLock().lock();
                try {
                    BackendManager.this.connections.values().forEach(player -> {
                        handler.sendPacket(new PacketPlayOutPlayerJoin(player.getUniqueId(), player.getAddress(), player.getVersion()));
                        handler.sendPacket(new PacketPlayOutServerSwitch(player.getUniqueId(), player.getServer()));
                    });
                }
                finally {
                    BackendManager.this.connectionLock.readLock().unlock();
                }
                handler.sendPacket(new PacketPlayOutSlots(PingManager.getInstance().getSlots()));
                ProxyServer.getInstance().getPluginManager().callEvent(new TelnetConnectEvent(handler));
            }

            @Override
            public void onDisconnect(TelnetHandler handler) {
                if (BackendManager.this.handler.equals(handler)) {
                    BackendManager.this.handler = null;
                }
                BackendManager.this.connectionLock.writeLock().lock();
                try {
                    BackendManager.this.connections.values().forEach(player -> {
                        if (!player.isLocal()) {
                            BackendManager.this.connections.remove(player.getUniqueId());
                            BackendManager.this.connectionsByName.remove(player.getName());
                        }
                    });
                }
                finally {
                    BackendManager.this.connectionLock.writeLock().unlock();
                }
                ProxyServer.getInstance().getPluginManager().callEvent(new TelnetDisconnectEvent(handler));
            }
        });
        this.client.setKeepAlive(true);
        this.client.connect();
        this.task = ProxyServer.getInstance().getScheduler().schedule(Backend.getInstance(), () -> {
            PacketPlayOutPlayers packet = new PacketPlayOutPlayers();
            for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
                packet.addPlayer(player.getUniqueId(), player.getPing());
            }
            this.sendPacket(packet);
        }, 3, 3, TimeUnit.SECONDS);
    }

    public void sendPacket(OutgoingPacket packet) {
        if (this.handler != null) {
            this.handler.sendPacket(packet);
        }
    }

    public void putLogin(UUID uuid, Document data) {
        this.loginLock.writeLock().lock();
        try {
            this.logins.put(uuid, data);
        }
        finally {
            this.loginLock.writeLock().unlock();
        }
    }

    public Document getLogin(UUID uuid) {
        this.loginLock.writeLock().lock();
        try {
            Document document = this.logins.remove(uuid);
            return document;
        }
        finally {
            this.loginLock.writeLock().unlock();
        }
    }

    public BackendPlayer getPlayer(String name) {
        this.connectionLock.readLock().lock();
        try {
            BackendPlayer online = this.connectionsByName.get(name);
            if (online != null) {
                return online;
            }
            Document document = this.loadPlayerData(name);
            if (document != null) {
                return new BackendPlayer(document);
            }
        }
        finally {
            this.connectionLock.readLock().unlock();
        }
        return null;
    }

    public BackendPlayer getPlayer(UUID uuid) {
        this.connectionLock.readLock().lock();
        try {
            BackendPlayer online = this.connections.get(uuid);
            if (online != null) {
                return online;
            }
            Document document = this.loadPlayerData(uuid);
            if (document != null) {
                return new BackendPlayer(document);
            }
        }
        finally {
            this.connectionLock.readLock().unlock();
        }
        return null;
    }

    public BackendConsole getConsole() {
        return new BackendConsole(ProxyServer.getInstance().getConsole());
    }

    public BackendSender getBackendSender(final UUID uuid) {
        return uuid.equals(getConsole().getUniqueId()) ? getConsole() : getPlayer(uuid);
    }

    public BackendSender getBackendSender(final CommandSender sender) {
        return (sender instanceof ProxiedPlayer) ? getPlayer(sender.getName()) : getConsole();
    }

    public Collection<BackendPlayer> getPlayers() {
        this.connectionLock.readLock().lock();
        try {
            Collection<BackendPlayer> collection = Collections.unmodifiableCollection(new HashSet<>(this.connections.values()));
            return collection;
        }
        finally {
            this.connectionLock.readLock().unlock();
        }
    }

    public /* varargs */ List<BackendPlayer> getPlayers(UUID ... uuids) {
        ArrayList<BackendPlayer> list = new ArrayList<>();
        ArrayList<Bson> filters = new ArrayList<>();
        for (UUID uuid : uuids) {
            filters.add(Filters.eq("uuid", uuid.toString()));
        }
        for (Document document : MongoManager.getInstance().getPlayersCollection().find(Filters.or(filters))) {
            list.add(new BackendPlayer(document));
        }
        return list;
    }

    public /* varargs */ List<BackendPlayer> getTricketyPlayers(String ... names) {
        ArrayList<BackendPlayer> list = new ArrayList<>();
        ArrayList<Bson> filters = new ArrayList<>();
        for (String name : names) {
            filters.add(Filters.eq("name_lower", name.toLowerCase()));
        }
        for (Document document : MongoManager.getInstance().getPlayersCollection().find(Filters.or(filters))) {
            list.add(new BackendPlayer(document));
        }
        return list;
    }

    public void addPlayer(BackendPlayer player) {
        this.connectionLock.writeLock().lock();
        try {
            this.connections.put(player.getUniqueId(), player);
            this.connectionsByName.put(player.getName(), player);
        }
        finally {
            this.connectionLock.writeLock().unlock();
        }
    }

    public void removePlayer(BackendPlayer player) {
        this.connectionLock.writeLock().lock();
        try {
            this.connections.remove(player.getUniqueId());
            this.connectionsByName.remove(player.getName());
        }
        finally {
            this.connectionLock.writeLock().unlock();
        }
    }

    public void savePlayerData(Document document) {
        MongoManager.getInstance().getPlayersCollection().updateOne(
                Filters.eq("uuid", document.getString("uuid")),
                new Document("$set", document),
                new UpdateOptions().upsert(true)
        );
    }

    public Document loadPlayerData(String name) {
        return MongoManager.getInstance().getPlayersCollection().find(Filters.eq("name_lower", name.toLowerCase())).first();
    }

    public Document loadPlayerData(UUID uuid) {
        return MongoManager.getInstance().getPlayersCollection().find(Filters.eq("uuid", uuid.toString())).first();
    }

    public UUID getDefaultUUID() {
        return new UUID(0, 0);
    }

    public void telnetBroadcastMessage(final String channel, final byte[] data) {
        this.telnetSendMessage(new ByteArrayDataOutput().writeByte((byte)0).toByteArray(), channel, data);
    }

    public void telnetProxyBroadcastMessage(final String channel, final byte[] data) {
        this.telnetSendMessage(new ByteArrayDataOutput().writeByte((byte)1).toByteArray(), channel, data);
    }

    public void telnetSpigotBroadcastMessage(final String channel, final byte[] data) {
        this.telnetSendMessage(new ByteArrayDataOutput().writeByte((byte)2).toByteArray(), channel, data);
    }

    public void telnetProxyRandomMessage(final String channel, final byte[] data) {
        this.telnetSendMessage(new ByteArrayDataOutput().writeByte((byte)3).toByteArray(), channel, data);
    }

    public void telnetSpigotRandomMessage(final String channel, final byte[] data) {
        this.telnetSendMessage(new ByteArrayDataOutput().writeByte((byte)4).toByteArray(), channel, data);
    }

    public void telnetProxyWherePlayerMessage(final BackendPlayer player, final String channel, final byte[] data) {
        this.telnetSendMessage(new ByteArrayDataOutput().writeByte((byte)5).writeUUID(player.getUniqueId()).toByteArray(), channel, data);
    }

    public void telnetSpigotWherePlayerMessage(final BackendPlayer player, final String channel, final byte[] data) {
        this.telnetSendMessage(new ByteArrayDataOutput().writeByte((byte)6).writeUUID(player.getUniqueId()).toByteArray(), channel, data);
    }

    public void telnetSendMessage(final String server, final String channel, final byte[] data) {
        this.telnetSendMessage(new ByteArrayDataOutput().writeByte((byte)7).writeString(server).toByteArray(), channel, data);
    }

    private void telnetSendMessage(final byte[] target, final String channel, final byte[] data) {
        final byte[] output = new ByteArrayDataOutput().writeString(channel).writeByteArray(data).toByteArray();
        this.sendPacket(new PacketPlayOutCustomMessage(target, "api", output));
    }
}