package me.doktormedrasen.backend.bungee.api.manager;

import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import me.doktormedrasen.backend.bungee.Backend;
import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import me.doktormedrasen.backend.bungee.api.events.AsynchronousLoginEvent;
import me.doktormedrasen.backend.bungee.api.events.PostLoginEvent;
import me.doktormedrasen.backend.bungee.api.message.EnglishMessages;
import me.doktormedrasen.backend.bungee.api.message.GermanMessages;
import me.doktormedrasen.backend.bungee.api.packets.PacketPlayOutCustomMessage;
import me.doktormedrasen.backend.bungee.api.packets.PacketPlayOutPlayerJoin;
import me.doktormedrasen.backend.utils.ByteArrayDataInput;
import me.doktormedrasen.backend.utils.ByteArrayDataOutput;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.*;

/**
 * Class by DoktorMedRasen
 */

@Getter
public class LoginManager {

    @Getter
    private static LoginManager instance;
    private boolean maintenance;
    private String reason;

    public LoginManager() {
        instance = this;
        this.load();
        Backend.getInstance().enableModule(this);
    }

    public void disable() {
        instance = null;
        Backend.getInstance().disableModule(this);
    }

    private void load() {
        Document document = MongoManager.getInstance().getMaintenanceCollection().find(Filters.eq("proxy", Backend.getInstance().getProxyName())).first();
        if (document == null) {
            MongoManager.getInstance().getMaintenanceCollection().insertOne(new Document("proxy", Backend.getInstance().getProxyName()).append("value", true).append("reason", "Maintenance"));
            this.maintenance = true;
            this.reason = "Maintenance";
        } else {
            this.maintenance = document.getBoolean("value");
            this.reason = document.getString("reason");
        }
        if (this.maintenance) {
            this.disconnectPlayers();
        }
    }

    public Date parseFirstSeen(Document document) {
        if (document != null && document.containsKey("firstseen")) {
            return document.getDate("firstseen");
        }
        return null;
    }

    public Date parseLastSeen(Document document) {
        if (document != null && document.containsKey("lastseen")) {
            return document.getDate("lastseen");
        }
        return null;
    }

    public void runAsynchronousLogin(AsynchronousLoginEvent event) {
        Document host = MongoManager.getInstance().getHostsCollection().find(Filters.eq("host", event.getLogin().getAddress().getHostName())).first();
        boolean bypassing = false;
        if (host != null) {
            List<UUID> uuids = new ArrayList<>();
            for (String s : host.get("accounts", new ArrayList<String>())) {
                uuids.add(UUID.fromString(s));
            }
            List<BackendPlayer> players = BackendManager.getInstance().getPlayers(uuids.toArray(new UUID[uuids.size()]));
            for (BackendPlayer player : players) {
                if (!player.isBanned()) continue;
                bypassing = true;
            }
            if (!uuids.contains(event.getLogin().getUniqueId())) {
                MongoManager.getInstance().getHostsCollection().updateOne(Filters.eq("host", event.getLogin().getAddress().getHostName()), new Document("$push", new Document("accounts", event.getLogin().getUniqueId().toString())), new UpdateOptions().upsert(true));
            }
        } else {
            MongoManager.getInstance().getHostsCollection().insertOne(new Document("host", event.getLogin().getAddress().getHostName()).append("accounts", new ArrayList<String>(Collections.singletonList(event.getLogin().getUniqueId().toString()))));
        }
        if (event.getLogin().getPlayerData().isBanned()) {
            event.getLogin().getPlayerData().getBan().setDenied(event.getLogin().getPlayerData().getBan().getDenied() + 1);
            PunishmentManager.getInstance().saveAndUpdateBan(event.getLogin().getUniqueId(), event.getLogin().getPlayerData().getBan());
            event.setCancelled(event.getLogin().getPlayerData().language(GermanMessages.BANNED(event.getLogin().getPlayerData().getBan().getReason(), event.getLogin().getPlayerData().getBan().getDurationStringGerman("§c")), EnglishMessages.BANNED(event.getLogin().getPlayerData().getBan().getReason(), event.getLogin().getPlayerData().getBan().getDurationStringEnglish("§c"))));
        } else if (bypassing) {
            event.setCancelled(event.getLogin().getPlayerData().language(GermanMessages.PUNISHMENT_BYPASS, EnglishMessages.PUNISHMENT_BYPASS));
        } else if (this.maintenance) {
            if (!event.getLogin().getPlayerData().hasPermission("maintenance.allowjoin")) {
                event.setCancelled(event.getLogin().getPlayerData().language(GermanMessages.MAINTENANCE_FORBIDDEN(this.reason), EnglishMessages.MAINTENANCE_FORBIDDEN(this.reason)));
            }
        } else if (BackendManager.getInstance().getPlayers().size() > 50 && !event.getLogin().getPlayerData().hasPermission("server.joinfull")) {
            event.setCancelled(event.getLogin().getPlayerData().language(GermanMessages.SERVER_FULL, EnglishMessages.SERVER_FULL));
        }
        ProxyServer.getInstance().getPluginManager().callEvent(event);
    }

    public void runPostLogin(ProxiedPlayer proxiedPlayer, Document document) {
        BackendPlayer player = new BackendPlayer(document);
        player.setConnection(proxiedPlayer);
        player.setProxy(Backend.getInstance().getProxyName());
        BackendManager.getInstance().addPlayer(player);
        ProxyServer.getInstance().getPluginManager().callEvent(new PostLoginEvent(player));
        BackendManager.getInstance().sendPacket(new PacketPlayOutPlayerJoin(player.getUniqueId(), player.getAddress(), player.getVersion()));
        ProxyServer.getInstance().getScheduler().runAsync(Backend.getInstance(), () -> StatusManager.getInstance().setOnline(player));
    }

    public void registerPlayer(UUID uuid) {
        MongoManager.getInstance().getPlayersCollection().updateOne(Filters.and((Bson[])new Bson[]{Filters.eq("uuid", uuid.toString()), Filters.exists("firstseen", false)}), new Document("$set", new Document("firstseen", new Date()).append("lastseen", new Date())));
    }

    public void logLogin(UUID uuid, Login login) {
        Document document = new Document("date", login.getDate()).append("ip", login.getIp()).append("version", login.getVersion()).append("target", login.getTarget()).append("proxy", login.getProxy()).append("result", login.getResult());
        MongoManager.getInstance().getLoginsCollection().updateOne(
                Filters.eq("uuid", uuid.toString()),
                new Document("$push", new Document("logins", document)), new UpdateOptions().upsert(true)
        );
    }

    public void logLogout(UUID uuid, Logout logout) {
        Document document = new Document("date", logout.getDate()).append("from", logout.getFrom());
        MongoManager.getInstance().getLoginsCollection().updateOne(
                Filters.eq("uuid", uuid.toString()),
                new Document("$push", new Document("logouts", document)), new UpdateOptions().upsert(true)
        );
    }

    public void setLastSeen(BackendPlayer player) {
        MongoManager.getInstance().getPlayersCollection().updateOne(
                Filters.eq("uuid", player.getUniqueId().toString()),
                new Document("$set", new Document("lastseen", new Date()))
        );
    }

    public List<Login> getLogins(BackendPlayer player) {
        Document document = MongoManager.getInstance().getLoginsCollection().find(Filters.eq("uuid", player.getUniqueId().toString())).first();
        if (document == null || !document.containsKey("logins")) {
            return new ArrayList<>();
        }
        List<Login> list = new ArrayList<>();
        for (Document entry : document.get("logins", Document[].class)) {
            list.add(new Login(entry.getDate("date"), entry.getString("ip"), entry.getInteger("version"), entry.getString("target"), entry.getString("proxy"), entry.getString("result")));
        }
        return list;
    }

    public List<Logout> getLogouts(BackendPlayer player) {
        Document document = MongoManager.getInstance().getLoginsCollection().find(Filters.eq("uuid", player.getUniqueId().toString())).first();
        if (document == null || !document.containsKey("logouts")) {
            return new ArrayList<>();
        }
        List<Logout> list = new ArrayList<>();
        for (Document entry : document.get("logouts", Document[].class)) {
            list.add(new Logout(entry.getDate("date"), entry.getString("from")));
        }
        return list;
    }

    public void setMaintenance(boolean maintenance, String reason, boolean global) {
        this.maintenance = maintenance;
        if (global) {
            MongoManager.getInstance().getMaintenanceCollection().updateMany(Filters.exists("value"), new Document("$set", new Document("value", maintenance).append("reason", reason)));
        } else {
            MongoManager.getInstance().getMaintenanceCollection().updateOne(Filters.eq("proxy", Backend.getInstance().getProxyName()), new Document("$set", new Document("value", maintenance).append("reason", reason)));
        }
        this.sendUpdate();
        if (maintenance) {
            this.disconnectPlayers();
        }
    }

    public void disconnectPlayers() {
        for (BackendPlayer player : BackendManager.getInstance().getPlayers()) {
            if (!player.isLocal() || player.hasPermission("maintenance.allowjoin")) continue;
            player.disconnect(player.language(GermanMessages.MAINTENANCE_KICK, EnglishMessages.MAINTENANCE_KICK));
        }
    }

    private void sendUpdate() {
        ByteArrayDataOutput output = new ByteArrayDataOutput();
        output.writeString("update");
        PacketPlayOutCustomMessage packet = new PacketPlayOutCustomMessage(new ByteArrayDataOutput().writeByte((byte) 1).toByteArray(), "loginmanager", output.toByteArray());
        BackendManager.getInstance().sendPacket(packet);
    }

    public void onRecieve(ByteArrayDataInput input) {
        String method = input.readString();
        if (method.equals("update")) {
            this.load();
        }
    }

    @Getter
    @Setter
    @AllArgsConstructor
    public static class Login {

        private Date date;
        private String ip;
        private int version;
        private String target;
        private String proxy;
        private String result;
    }

    @Getter
    @Setter
    @AllArgsConstructor
    public static class Logout {

        private Date date;
        private String from;
    }

}