package me.doktormedrasen.backend.bungee.api;

import me.doktormedrasen.backend.bungee.api.message.BackendComponent;
import net.md_5.bungee.api.chat.BaseComponent;

import java.util.UUID;

public interface BackendSender {

    public String getName();

    public UUID getUniqueId();

    public String getDisplayName();

    public void sendMessage(BaseComponent var1);

    public void sendMessage(String var1);

    public void sendMessage(String var1, String var2);

    public void sendMessage(BaseComponent var1, BaseComponent var2);

    public void sendMessage(BackendComponent var1);

    public void sendMessage(BackendComponent var1, BackendComponent var2);

    public boolean hasPermission(String var1);

    public String language(String var1, String var2);

}
