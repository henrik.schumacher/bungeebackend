package me.doktormedrasen.backend.bungee.api.packets;

import me.doktormedrasen.backend.telnet.OutgoingPacket;
import me.doktormedrasen.backend.telnet.PacketDataSerializer;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Class by DoktorMedRasen :P
 */

public class PacketPlayOutPlayers implements OutgoingPacket {

    private Map<UUID, Integer> players = new HashMap<>();

    public void addPlayer(UUID uuid, int latency) {
        this.players.put(uuid, latency);
    }

    public void encode(PacketDataSerializer serializer) {
        serializer.writeInt(this.players.size());
        for (UUID uuid : this.players.keySet()) {
            serializer.writeUUID(uuid);
            serializer.writeInt(this.players.get(uuid));
        }
    }

}
