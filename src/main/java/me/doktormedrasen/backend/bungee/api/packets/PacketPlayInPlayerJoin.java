package me.doktormedrasen.backend.bungee.api.packets;

import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import me.doktormedrasen.backend.bungee.api.manager.BackendManager;
import me.doktormedrasen.backend.telnet.IncomingPacket;
import me.doktormedrasen.backend.telnet.PacketDataSerializer;
import me.doktormedrasen.backend.telnet.TelnetHandler;

import java.net.InetSocketAddress;
import java.util.UUID;

/**
 * Class by DoktorMedRasen :P
 */

public class PacketPlayInPlayerJoin implements IncomingPacket {

    private UUID uuid;
    private String proxy;
    private InetSocketAddress address;
    private int version;

    public void decode(PacketDataSerializer serializer) {
        this.uuid = serializer.readUUID();
        this.proxy = serializer.readString();
        this.address = new InetSocketAddress(serializer.readString(), serializer.readInt());
        this.version = serializer.readInt();
    }

    public void handle(TelnetHandler handler) {
        BackendPlayer player = BackendManager.getInstance().getPlayer(this.uuid);
        player.setProxy(this.proxy);
        player.setAddress(this.address);
        player.setVersion(this.version);
        BackendManager.getInstance().addPlayer(player);
    }

}