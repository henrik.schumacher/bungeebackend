package me.doktormedrasen.backend.bungee.api.punishment;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

/**
 * Class by DoktorMedRasen
 */

@Getter
@Setter
public class BackendMute extends BackendPunishment {

    private String evidence;
    private String writtenMessage;

    public BackendMute(UUID executor, String reason, Date date, Date until, String evidence, String writtenMessage, int denied) {
        super(executor, reason, date, until, denied);
        this.evidence = evidence;
        this.writtenMessage = writtenMessage;
    }

}
