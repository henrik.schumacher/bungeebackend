package me.doktormedrasen.backend.bungee.api.events.telnet;
;
import lombok.Getter;
import me.doktormedrasen.backend.bungee.api.packets.PacketPlayOutCustomMessage;
import me.doktormedrasen.backend.telnet.OutgoingPacket;
import me.doktormedrasen.backend.telnet.TelnetHandler;
import me.doktormedrasen.backend.utils.ByteArrayDataInput;
import me.doktormedrasen.backend.utils.ByteArrayDataOutput;
import net.md_5.bungee.api.plugin.Event;

/**
 * Class by DoktorMedRasen
 */

@Getter
public class TelnetMessageRecieveEvent extends Event {

    private TelnetHandler handler;
    private String sender;
    private String channel;
    private byte[] data;

    public TelnetMessageRecieveEvent(TelnetHandler handler, String sender, ByteArrayDataInput input) {
        this.handler = handler;
        this.sender = sender;
        this.channel = input.readString();
        this.data = input.readByteArray();
    }

    public void reply(byte[] data) {
        ByteArrayDataOutput output = new ByteArrayDataOutput();
        output.writeString(this.channel);
        output.writeByteArray(data);
        ByteArrayDataOutput target = new ByteArrayDataOutput().writeByte((byte)7).writeString(this.sender);
        this.handler.sendPacket(new PacketPlayOutCustomMessage(target.toByteArray(), "api", output.toByteArray()));
    }

}