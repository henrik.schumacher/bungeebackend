package me.doktormedrasen.backend.bungee.api.events;

import lombok.Getter;
import lombok.Setter;
import me.doktormedrasen.backend.bungee.api.PendingLogin;
import net.md_5.bungee.api.plugin.Event;

/**
 * Class by DoktorMedRasen
 */

public class AsynchronousLoginEvent extends Event {

    @Getter
    private PendingLogin login;
    @Setter
    private String cancelled;

    public AsynchronousLoginEvent(PendingLogin login) {
        this.login = login;
    }

    public boolean isCancelled() {
        return this.cancelled != null;
    }

    public String getCancelledReason() {
        return this.cancelled;
    }

}
