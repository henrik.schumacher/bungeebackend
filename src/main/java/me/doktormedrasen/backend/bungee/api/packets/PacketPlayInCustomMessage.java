package me.doktormedrasen.backend.bungee.api.packets;

import me.doktormedrasen.backend.bungee.api.events.telnet.TelnetMessageRecieveEvent;
import me.doktormedrasen.backend.bungee.api.manager.*;
import me.doktormedrasen.backend.telnet.IncomingPacket;
import me.doktormedrasen.backend.telnet.PacketDataSerializer;
import me.doktormedrasen.backend.telnet.TelnetHandler;
import me.doktormedrasen.backend.utils.ByteArrayDataInput;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Event;

/**
 * Class by DoktorMedRasen
 */

public class PacketPlayInCustomMessage implements IncomingPacket {

    private String sender;
    private String channel;
    private byte[] message;

    public void decode(PacketDataSerializer serializer) {
        this.sender = serializer.readString();
        this.channel = serializer.readString();
        this.message = serializer.readBytes(serializer.readInt());
    }

    public void handle(TelnetHandler handler) {
        ByteArrayDataInput input = new ByteArrayDataInput(this.message);
        switch (this.channel) {
            case "api": {
                ProxyServer.getInstance().getPluginManager().callEvent((Event) new TelnetMessageRecieveEvent(handler, this.sender, input));
                break;
            }
            case "punishmentmanager": {
                PunishmentManager.getInstance().onRecieve(input);
                break;
            }
            case "languagemanager": {
                LanguageManager.getInstance().onRecieve(input);
                break;
            }
            case "loginmanager": {
                LoginManager.getInstance().onRecieve(input);
                break;
            }
            case "permissionmanager": {
                PermissionManager.getInstance().onRecieve(input);
                break;
            }
            case "pingmanager": {
                PingManager.getInstance().onRecieve(input);
                break;
            }
            case "tabmanager": {
                TabManager.getInstance().onRecieve(input);
                break;
            }
        }
    }

}
