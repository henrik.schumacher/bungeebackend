package me.doktormedrasen.backend.bungee.api.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import net.md_5.bungee.api.plugin.Event;

/**
 * Class by DoktorMedRasen
 */

@Getter
@AllArgsConstructor
public class PlayerDisconnectEvent extends Event {

    private BackendPlayer player;

}
