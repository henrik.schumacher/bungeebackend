package me.doktormedrasen.backend.bungee.api;

import lombok.Getter;
import lombok.Setter;
import me.doktormedrasen.backend.bungee.api.manager.BackendManager;
import me.doktormedrasen.backend.bungee.api.manager.StatusManager;
import me.doktormedrasen.backend.bungee.api.message.BackendComponent;
import me.doktormedrasen.backend.bungee.api.packets.PacketPlayOutConnectToServer;
import me.doktormedrasen.backend.bungee.api.packets.PacketPlayOutKickPlayer;
import me.doktormedrasen.backend.bungee.api.packets.PacketPlayOutSendMessage;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.bson.Document;

import java.net.InetSocketAddress;
import java.util.UUID;

/**
 * Class by DoktorMedRasen
 */

@Getter
@Setter
public class BackendPlayer extends PlayerData implements BackendSender {

    private ProxiedPlayer connection;
    private InetSocketAddress address;
    private int version;
    private int latency;
    private String proxy;
    private String server;

    public BackendPlayer(Document document) {
        super(document);
        this.proxy = StatusManager.getInstance().parseProxy(document);
        this.server = StatusManager.getInstance().parseServer(document);
    }

    @Override
    public UUID getUniqueId() {
        return super.getUniqueId();
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public String getDisplayName() {
        return super.getDisplayName();
    }

    public boolean isLocal() {
        return this.connection != null;
    }

    public boolean isOnline() {
        return this.proxy != null;
    }

    public void setConnection(ProxiedPlayer connection) {
        this.connection = connection;
        this.address = connection.getAddress();
        this.version = connection.getPendingConnection().getVersion();
    }

    public boolean isOnlineMode() {
        return this.connection.getPendingConnection().isOnlineMode();
    }

    public double getVersionTranslated() {
        if (this.version <= 5) {
            return 1.7;
        }
        if (this.version <= 47) {
            return 1.8;
        }
        if (this.version <= 110) {
            return 1.9;
        }
        if (this.version <= 210) {
            return 1.1;
        }
        if (this.version <= 316) {
            return 1.11;
        }
        if (this.version <= 340) {
            return 1.12;
        }
        return 0.0;
    }

    public void connect(String server) {
        if (this.isLocal()) {
            ServerInfo info = ProxyServer.getInstance().getServerInfo(server);
            if (info != null) {
                this.connection.connect(info);
            }
        } else {
            BackendManager.getInstance().sendPacket(new PacketPlayOutConnectToServer(this.getUniqueId(), server));
        }
    }

    public void disconnect(String message) {
        if (this.isLocal()) {
            this.connection.disconnect(new TextComponent(message));
        } else {
            BackendManager.getInstance().sendPacket(new PacketPlayOutKickPlayer(this.getUniqueId(), message));
        }
    }

    @Override
    public void sendMessage(BaseComponent message) {
        if (this.isLocal()) {
            this.connection.sendMessage(message);
        } else {
            BackendManager.getInstance().sendPacket(new PacketPlayOutSendMessage(this.getUniqueId(), message.toString()));
        }
    }

    @Override
    public void sendMessage(String message) {
        this.sendMessage(new TextComponent(message));
    }

    @Override
    public void sendMessage(String german, String english) {
        this.sendMessage(new TextComponent(this.getLanguage().equals(BackendLanguage.GERMAN) ? german : english));
    }

    @Override
    public void sendMessage(BaseComponent german, BaseComponent english) {
        this.sendMessage(this.getLanguage().equals(BackendLanguage.GERMAN) ? german : english);
    }

    @Override
    public void sendMessage(BackendComponent component) {
        this.sendMessage(component.getComponent());
    }

    @Override
    public void sendMessage(BackendComponent german, BackendComponent english) {
        this.sendMessage(this.getLanguage().equals(BackendLanguage.GERMAN) ? german.getComponent() : english.getComponent());
    }

    @Override
    public String language(String german, String english) {
        return this.getLanguage().equals(BackendLanguage.GERMAN) ? german : english;
    }

}
