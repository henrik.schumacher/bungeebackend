package me.doktormedrasen.backend.bungee.api.packets;

import lombok.AllArgsConstructor;
import me.doktormedrasen.backend.telnet.OutgoingPacket;
import me.doktormedrasen.backend.telnet.PacketDataSerializer;

/**
 * Class by DoktorMedRasen
 */

@AllArgsConstructor
public class PacketPlayOutCustomMessage implements OutgoingPacket {

    private byte[] target;
    private String channel;
    private byte[] message;

    public void encode(PacketDataSerializer serializer) {
        serializer.writeInt(this.target.length);
        serializer.writeBytes(this.target);
        serializer.writeString(this.channel);
        serializer.writeInt(this.message.length);
        serializer.writeBytes(this.message);
    }

    public static class MessageTarget {
        public static final byte BROADCAST = 0;
        public static final byte PROXY_BROADCAST = 1;
        public static final byte SPIGOT_BROADCAST = 2;
        public static final byte PROXY_RANDOM = 3;
        public static final byte SPIGOT_RANDOM = 4;
        public static final byte PLAYER_PROXY = 5;
        public static final byte PLAYER_SPIGOT = 6;
        public static final byte SERVER = 7;
    }

}
