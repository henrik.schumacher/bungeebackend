package me.doktormedrasen.backend.bungee.api.packets;

import lombok.AllArgsConstructor;
import me.doktormedrasen.backend.telnet.OutgoingPacket;
import me.doktormedrasen.backend.telnet.PacketDataSerializer;

/**
 * Class by DoktorMedRasen :P
 */

@AllArgsConstructor
public class PacketPlayOutSlots implements OutgoingPacket {

    private int slots;

    public void encode(PacketDataSerializer serializer) {
        serializer.writeInt(this.slots);
    }

}
