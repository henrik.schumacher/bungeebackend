package me.doktormedrasen.backend.bungee.api.punishment;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

/**
 * Class by DoktorMedRasen
 */

@Getter
@Setter
public class BackendBan extends BackendPunishment {

    private String evidence;

    public BackendBan(UUID executor, String reason, Date date, Date until, String evidence, int denied) {
        super(executor, reason, date, until, denied);
        this.evidence = evidence;
    }

}
