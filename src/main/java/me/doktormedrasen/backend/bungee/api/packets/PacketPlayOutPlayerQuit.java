package me.doktormedrasen.backend.bungee.api.packets;

import lombok.AllArgsConstructor;
import me.doktormedrasen.backend.telnet.OutgoingPacket;
import me.doktormedrasen.backend.telnet.PacketDataSerializer;

import java.util.UUID;

/**
 * Class by DoktorMedRasen :P
 */

@AllArgsConstructor
public class PacketPlayOutPlayerQuit implements OutgoingPacket {

    private UUID uuid;

    public void encode(PacketDataSerializer serializer) {
        serializer.writeUUID(uuid);
    }

}
