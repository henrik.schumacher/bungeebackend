package me.doktormedrasen.backend.bungee.api.message;

import lombok.Getter;
import net.md_5.bungee.api.chat.*;

/**
 * Class by DoktorMedRasen
 */

@Getter
public class BackendComponent {

    private String text;
    private TextComponent component;

    public BackendComponent(String text) {
        this.text = text;
        this.component = new TextComponent(text);
    }

    public void setClickEvent(ClickEvent.Action action, String value) {
        this.component.setClickEvent(new ClickEvent(action, value));
    }

    public void setHoverEvent(HoverEvent.Action action, String value) {
        this.component.setHoverEvent(new HoverEvent(action, new ComponentBuilder(value).create()));
    }

    public BackendComponent add(String text) {
        this.component.addExtra(text);
        return this;
    }

    public BackendComponent add(BaseComponent component) {
        this.component.addExtra(component);
        return this;
    }

    public BackendComponent add(BackendComponent component) {
        this.component.addExtra(component.getComponent());
        return this;
    }

}