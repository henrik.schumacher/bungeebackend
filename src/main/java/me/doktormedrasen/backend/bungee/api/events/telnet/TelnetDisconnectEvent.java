package me.doktormedrasen.backend.bungee.api.events.telnet;

import lombok.AllArgsConstructor;
import lombok.Getter;
import me.doktormedrasen.backend.telnet.TelnetHandler;
import net.md_5.bungee.api.plugin.Event;

/**
 * Class by DoktorMedRasen
 */

@Getter
@AllArgsConstructor
public class TelnetDisconnectEvent extends Event {

    private TelnetHandler handler;

}
