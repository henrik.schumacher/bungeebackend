package me.doktormedrasen.backend.bungee.api.manager;

import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.model.Updates;
import lombok.Getter;
import me.doktormedrasen.backend.bungee.Backend;
import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import me.doktormedrasen.backend.bungee.api.BackendSender;
import me.doktormedrasen.backend.bungee.api.packets.PacketPlayOutCustomMessage;
import me.doktormedrasen.backend.bungee.api.punishment.BackendBan;
import me.doktormedrasen.backend.bungee.api.punishment.BackendLog;
import me.doktormedrasen.backend.bungee.api.punishment.BackendMute;
import me.doktormedrasen.backend.bungee.api.punishment.BackendPunishment;
import me.doktormedrasen.backend.utils.ByteArrayDataInput;
import me.doktormedrasen.backend.utils.ByteArrayDataOutput;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Class by DoktorMedRasen
 */

public class PunishmentManager {

    @Getter
    private static PunishmentManager instance;

    public PunishmentManager() {
        instance = this;
        Backend.getInstance().enableModule(this);
    }

    public void disable() {
        instance = null;
        Backend.getInstance().disableModule(this);
    }

    public BackendBan parseBan(Document document) {
        if (document != null && document.containsKey("ban")) {
            Document ban = document.get("ban", Document.class);
            if (ban == null) {
                return null;
            }
            return new BackendBan(UUID.fromString(ban.getString("executor")), ban.getString("reason"), ban.getDate("date"), ban.getDate("until"), ban.getString("evidence"), ban.getInteger("denied"));
        }
        return null;
    }

    public BackendMute parseMute(Document document) {
        if (document != null && document.containsKey("mute")) {
            Document mute = document.get("mute", Document.class);
            if (mute == null) {
                return null;
            }
            return new BackendMute(UUID.fromString(mute.getString("executor")), mute.getString("reason"), mute.getDate("date"), mute.getDate("until"), mute.getString("evidence"), mute.getString("writtenMessage"), mute.getInteger("denied"));
        }
        return null;
    }

    public void saveAndUpdateBan(UUID uuid, BackendBan ban) {
        if (ban != null) {
            Document document = new Document("executor", ban.getExecutor()).append("reason", ban.getReason()).append("date", ban.getDate()).append("until", ban.getUntil()).append("evidence", ban.getEvidence()).append("denied", ban.getDenied());
            MongoManager.getInstance().getPlayersCollection().updateOne(
                    Filters.eq("uuid", uuid.toString()),
                    Updates.combine(
                            Updates.set("ban", document)
                    )
            );
            this.sendPlayerUpdate(uuid);
        }
    }

    public void saveAndUpdateMute(UUID uuid, BackendMute mute) {
        if (mute != null) {
            Document document = new Document("executor", mute.getExecutor()).append("reason", mute.getReason()).append("date", mute.getDate()).append("until", mute.getUntil()).append("evidence", mute.getEvidence()).append("writtenMessage", mute.getWrittenMessage()).append("denied", mute.getDenied());
            MongoManager.getInstance().getPlayersCollection().updateOne(
                    Filters.eq("uuid", uuid.toString()),
                    Updates.combine(
                            Updates.set("mute", document)
                    )
            );
            this.sendPlayerUpdate(uuid);
        }
    }

    public List<BackendLog> loadPunishmentLog(UUID uuid) {
        List<BackendLog> list = new ArrayList<>();
        Document document = MongoManager.getInstance().getPunishmentsCollection().find(Filters.eq("uuid", uuid.toString())).first();
        if (document == null) {
            return list;
        }
        for (Document entry : document.get("punishments", Document[].class)) {
            Document punishment = entry.get("punishment", Document.class);
            BackendLog.LogType type = BackendLog.LogType.valueOf(entry.getString("type"));
            BackendPunishment backendPunishment;
            if (type.equals(BackendLog.LogType.BAN) || type.equals(BackendLog.LogType.UNBAN)) {
                backendPunishment = new BackendBan(UUID.fromString(punishment.getString("executor")), punishment.getString("reason"), punishment.getDate("date"), punishment.getDate("until"), punishment.getString("evidence"), -1);
            } else {
                backendPunishment = new BackendMute(UUID.fromString(document.getString("executor")), document.getString("reason"), document.getDate("date"), document.getDate("until"), document.getString("evidence"), document.getString("writtenMessage"), -1);
            }
            BackendLog log = new BackendLog(type, backendPunishment, UUID.fromString(entry.getString("executor")), entry.getString("reason"), entry.getDate("date"));
            list.add(log);
        }
        return list;
    }

    public void logPunishment(UUID uuid, BackendPunishment punishment) {
        BackendLog log = new BackendLog(punishment instanceof BackendBan ? BackendLog.LogType.BAN : BackendLog.LogType.MUTE, punishment, punishment.getExecutor(), punishment.getReason(), punishment.getDate());
        Document entry = new Document("executor", punishment.getExecutor().toString()).append("reason", punishment.getReason()).append("date", punishment.getDate()).append("until", punishment.getUntil());
        if (punishment instanceof BackendBan) {
            entry.append("evidence", ((BackendBan) punishment).getEvidence());
        } else if (punishment instanceof BackendMute) {
            entry.append("evidence", ((BackendMute) punishment).getEvidence());
            entry.append("writtenMessage", ((BackendMute) punishment).getWrittenMessage());
        }
        entry.append("denied", punishment.getDenied());
        Document document = new Document("type", log.getLogType()).append("executor", log.getExecutor()).append("reason", log.getReason()).append("date", log.getDate());
        document.append("punishment", entry);
        MongoManager.getInstance().getPunishmentsCollection().updateOne(
                Filters.eq("uuid", uuid.toString()),
                new Document("$push", new Document("punishments", document)),
                new UpdateOptions().upsert(true)
        );
    }

    public void logPunishmentRemoval(UUID uuid, BackendPunishment punishment, BackendSender remover, String reason) {
        BackendLog log = new BackendLog(punishment instanceof BackendBan ? BackendLog.LogType.UNBAN : BackendLog.LogType.UNMUTE, punishment, remover.getUniqueId(), reason, new Date());
        Document entry = new Document("executor", punishment.getExecutor().toString()).append("reason", punishment.getReason()).append("date", punishment.getDate()).append("until", punishment.getUntil());
        if (punishment instanceof BackendBan) {
            entry.append("evidence", ((BackendBan) punishment).getEvidence());
        } else if (punishment instanceof BackendMute) {
            entry.append("evidence", ((BackendMute) punishment).getEvidence());
            entry.append("writtenMessage", ((BackendMute) punishment).getWrittenMessage());
        }
        entry.append("denied", punishment.getDenied());
        Document document = new Document("type", log.getLogType()).append("executor", log.getExecutor()).append("reason", log.getReason()).append("date", log.getDate());
        document.append("punishment", entry);
        MongoManager.getInstance().getPunishmentsCollection().updateOne(
                Filters.eq("uuid", uuid.toString()),
                new Document("$push", new Document("punishments", document)),
                new UpdateOptions().upsert(true)
        );
    }

    public void clearPunishmentLog(UUID uuid) {
        MongoManager.getInstance().getPlayersCollection().deleteOne(Filters.eq("uuid", uuid.toString()));
    }

    private void sendPlayerUpdate(UUID uuid) {
        ByteArrayDataOutput output = new ByteArrayDataOutput();
        output.writeUUID(uuid);
        PacketPlayOutCustomMessage packet = new PacketPlayOutCustomMessage(new ByteArrayDataOutput().writeByte((byte) 1).toByteArray(), "punishmentmanager", output.toByteArray());
        BackendManager.getInstance().sendPacket(packet);
    }

    public void onRecieve(ByteArrayDataInput input) {
        BackendPlayer player = BackendManager.getInstance().getPlayer(input.readUUID());
        if (player != null) {
            player.reloadPunishment();
        }
    }

}
