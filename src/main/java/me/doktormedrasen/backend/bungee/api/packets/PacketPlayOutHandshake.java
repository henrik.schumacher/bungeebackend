package me.doktormedrasen.backend.bungee.api.packets;

import lombok.AllArgsConstructor;
import me.doktormedrasen.backend.telnet.OutgoingPacket;
import me.doktormedrasen.backend.telnet.PacketDataSerializer;

/**
 * Class by DoktorMedRasen :P
 */

@AllArgsConstructor
public class PacketPlayOutHandshake implements OutgoingPacket {

    private String type;
    private String name;

    public void encode(PacketDataSerializer serializer) {
        serializer.writeString(this.type);
        serializer.writeString(this.name);
    }

}
