package me.doktormedrasen.backend.bungee.api.permissions;

import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.model.Updates;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import me.doktormedrasen.backend.bungee.api.manager.MongoManager;
import me.doktormedrasen.backend.bungee.api.manager.PermissionManager;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Class by DoktorMedRasen :P
 */

@Getter
@Setter
@AllArgsConstructor
public class PermissionGroup {

    private String name;
    private String displayName;
    private String tabPrefix;
    private String chatPrefix;
    private String sortValue;
    private List<String> permissions;
    private List<String> inheritances;
    private List<String> packages;

    public PermissionGroup(Document document) {
        if (document == null) return;
        this.name = document.getString("name");
        this.displayName = document.getString("displayName");
        this.tabPrefix = document.getString("tabPrefix");
        this.chatPrefix = document.getString("chatPrefix");
        this.sortValue = document.getString("sortValue");
        this.permissions = document.get("permissions", new ArrayList<>());
        this.inheritances = document.get("inheritances", new ArrayList<>());
        this.packages = document.get("packages", new ArrayList<>());
    }

    public static PermissionGroup load(String name) {
        return fromDocument(MongoManager.getInstance().getGroupsCollection().find(Filters.eq("name", name.toLowerCase())).first());
    }

    private static PermissionGroup fromDocument(Document document) {
        if (document == null) return null;

        return new PermissionGroup(
                document.getString("name"),
                document.getString("displayName"),
                document.getString("tabPrefix"),
                document.getString("chatPrefix"),
                document.getString("sortValue"),
                document.get("permissions", new ArrayList()),
                document.get("inheritances", new ArrayList()),
                document.get("packages", new ArrayList<>())
        );
    }

    public void save() {
        MongoManager.getInstance().getGroupsCollection().updateOne(
                Filters.eq("name", this.name),
                Updates.combine(
                        Updates.set("displayName", this.displayName),
                        Updates.set("tabPrefix", this.tabPrefix),
                        Updates.set("chatPrefix", this.chatPrefix),
                        Updates.set("sortValue", this.sortValue),
                        Updates.set("permissions", this.permissions),
                        Updates.set("inheritances", this.inheritances),
                        Updates.set("packages", this.packages)
                ),
                new UpdateOptions().upsert(true)
        );
    }

    public void reload() {
        Document document = PermissionManager.getInstance().loadGroup(this.name);
        if (document == null) return;
        this.displayName = document.getString("displayName");
        this.tabPrefix = document.getString("tabPrefix");
        this.chatPrefix = document.getString("chatPrefix");
        this.sortValue = document.getString("sortValue");
        this.permissions = document.get("permissions", new ArrayList<>());
        this.inheritances = new ArrayList<>();
        this.packages = new ArrayList<>();
        this.inheritances.addAll(document.get("inheritances", new ArrayList<>()));
        this.packages.addAll(document.get("packages", new ArrayList<>()));
    }

    public void delete() {
        MongoManager.getInstance().getGroupsCollection().deleteOne(Filters.eq("name", this.name));
    }

    public List<PermissionGroup> getInheritances() {
        List<PermissionGroup> list = new ArrayList<>();
        this.inheritances.forEach(inheritances -> list.add(PermissionGroup.load(inheritances)));
        return list;
    }

    public List<PermissionPackage> getPackages() {
        List<PermissionPackage> list = new ArrayList<>();
        this.packages.forEach(packages -> list.add(PermissionPackage.load(packages)));
        return list;
    }

    public boolean superHasPermission(String permission) {
        permission = permission.toLowerCase();
        boolean hasPermission = false;
        if (this.permissions.contains("*")) {
            hasPermission = true;
        }
        for (final PermissionGroup group : this.getInheritances()) {
            if (group.superContainsPermission("*")) {
                hasPermission = true;
            }
        }
        for (final PermissionPackage permissionPackage : this.getPackages()) {
            if (permissionPackage.getPermissions().contains("*")) {
                hasPermission = true;
            }
        }
        for (final PermissionGroup group : this.getInheritances()) {
            if (group.superContainsPermission(permission)) {
                hasPermission = true;
            }
            if (group.superContainsPermission("-" + permission)) {
                hasPermission = false;
            }
        }
        for (final PermissionPackage permissionPackage : this.getPackages()) {
            if (permissionPackage.getPermissions().contains(permission)) {
                hasPermission = true;
            }
            if (permissionPackage.getPermissions().contains("-" + permission)) {
                hasPermission = false;
            }
        }
        if (this.permissions.contains(permission)) {
            hasPermission = true;
        }
        if (this.permissions.contains("-" + permission)) {
            hasPermission = false;
        }
        return hasPermission;
    }

    public boolean superContainsPermission(final String permission) {
        for (final PermissionGroup group : this.getInheritances()) {
            if (group.superContainsPermission(permission)) {
                return true;
            }
        }
        for (final PermissionPackage permissionPackage : this.getPackages()) {
            if (permissionPackage.getPermissions().contains(permission)) {
                return true;
            }
        }
        return this.permissions.contains(permission);
    }

}
