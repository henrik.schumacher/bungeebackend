package me.doktormedrasen.backend.bungee.api;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.config.ListenerInfo;
import net.md_5.bungee.api.connection.Connection;
import net.md_5.bungee.api.connection.PendingConnection;
import org.bson.Document;

import java.net.InetSocketAddress;
import java.util.UUID;

/**
 * Class by DoktorMedRasen
 */

public class PendingLogin {

    private PendingConnection handler;
    private PlayerData playerData;

    public PendingLogin(PendingConnection handler, Document document) {
        this.handler = handler;
        this.playerData = new PlayerData(document);
    }

    public UUID getUniqueId() {
        return this.handler.getUniqueId();
    }

    public String getName() {
        return this.handler.getName();
    }

    public int getVersion() {
        return this.handler.getVersion();
    }

    public InetSocketAddress getVirtualHost() {
        return this.handler.getVirtualHost();
    }

    public ListenerInfo getListener() {
        return this.handler.getListener();
    }

    public boolean isOnlineMode() {
        return this.handler.isOnlineMode();
    }

    public void setOnlineMode(boolean b) {
        this.handler.setOnlineMode(b);
    }

    public boolean isLegacy() {
        return this.handler.isLegacy();
    }

    public InetSocketAddress getAddress() {
        return this.handler.getAddress();
    }

    public void disconnect(String s) {
        this.handler.disconnect(s);
    }

    public /* varargs */ void disconnect(BaseComponent ... baseComponents) {
        this.handler.disconnect(baseComponents);
    }

    public void disconnect(BaseComponent baseComponent) {
        this.handler.disconnect(baseComponent);
    }

    public boolean isConnected() {
        return this.handler.isConnected();
    }

    public Connection.Unsafe unsafe() {
        return this.handler.unsafe();
    }

    public PlayerData getPlayerData() {
        return this.playerData;
    }

}
