package me.doktormedrasen.backend.bungee.api.manager;

import lombok.Getter;
import me.doktormedrasen.backend.bungee.Backend;
import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import me.doktormedrasen.backend.bungee.api.PendingLogin;
import me.doktormedrasen.backend.bungee.api.events.AsynchronousLoginEvent;
import me.doktormedrasen.backend.bungee.api.packets.PacketPlayOutPlayerQuit;
import me.doktormedrasen.backend.bungee.api.permissions.PermissionGroup;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import org.bson.Document;

import java.util.Collections;
import java.util.Date;

/**
 * Class by DoktorMedRasen
 */

public class ConnectManager {

    @Getter
    private static ConnectManager instance;

    public ConnectManager() {
        instance = this;
        Backend.getInstance().enableModule(this);
    }

    public void disable() {
        instance = null;
        Backend.getInstance().disableModule(this);
    }

    public void onLogin(LoginEvent event) {
        /*if (BackendManager.getInstance().getPlayer(event.getConnection().getUniqueId()) != null || ProxyServer.getInstance().getPlayer(event.getConnection().getName()) != null) {
            event.setCancelled(true);
            event.setCancelReason(new TextComponent("§cYou are already connected to this proxy"));
            return;
        }*/
        event.registerIntent(Backend.getInstance());
        ProxyServer.getInstance().getScheduler().runAsync(Backend.getInstance(), () -> {
            Document document = BackendManager.getInstance().loadPlayerData(event.getConnection().getUniqueId());
            if (document == null) {
                document = new Document("uuid", event.getConnection().getUniqueId().toString()).append("name", event.getConnection().getName()).append("name_lower", event.getConnection().getName().toLowerCase());
            }
            if (!document.getString("name").equals(event.getConnection().getName())) {
                document.put("name", event.getConnection().getName());
            }
            if (!document.getString("name_lower").equals(event.getConnection().getName().toLowerCase())) {
                document.put("name", event.getConnection().getName().toLowerCase());
            }
            if (!document.containsKey("firstseen")) {
                document.put("firstseen", new Date());
            }
            document.put("lastseen", new Date());
            BackendManager.getInstance().savePlayerData(document);
            PendingLogin login = new PendingLogin(event.getConnection(), document);
            AsynchronousLoginEvent asynchronousLoginEvent = new AsynchronousLoginEvent(login);
            LoginManager.getInstance().runAsynchronousLogin(asynchronousLoginEvent);
            if (asynchronousLoginEvent.isCancelled()) {
                event.setCancelled(true);
                event.setCancelReason(new TextComponent(asynchronousLoginEvent.getCancelledReason()));
            } else {
                BackendManager.getInstance().putLogin(event.getConnection().getUniqueId(), BackendManager.getInstance().loadPlayerData(login.getUniqueId()));
            }
            event.completeIntent(Backend.getInstance());
            LoginManager.getInstance().logLogin(login.getUniqueId(), new LoginManager.Login(new Date(), event.getConnection().getAddress().getHostString(), event.getConnection().getVersion(), event.getConnection().getVirtualHost().toString(), Backend.getInstance().getProxyName(), asynchronousLoginEvent.isCancelled() ? asynchronousLoginEvent.getCancelledReason() : "success"));
        });
    }

    public void onPostLogin(PostLoginEvent event) {
        LoginManager.getInstance().runPostLogin(event.getPlayer(), BackendManager.getInstance().getLogin(event.getPlayer().getUniqueId()));
    }

    public void onDisconnect(PlayerDisconnectEvent event) {
        BackendPlayer player = BackendManager.getInstance().getPlayer(event.getPlayer().getUniqueId());
        if (player == null) {
            return;
        }
        ProxyServer.getInstance().getPluginManager().callEvent(new me.doktormedrasen.backend.bungee.api.events.PlayerDisconnectEvent(player));
        BackendManager.getInstance().removePlayer(player);
        BackendManager.getInstance().sendPacket(new PacketPlayOutPlayerQuit(player.getUniqueId()));
        ProxyServer.getInstance().getScheduler().runAsync(Backend.getInstance(), () -> {
           StatusManager.getInstance().setOffline(player);
           LoginManager.getInstance().setLastSeen(player);
           LoginManager.getInstance().logLogout(player.getUniqueId(), new LoginManager.Logout(new Date(), player.getServer()));
        });
    }

}
