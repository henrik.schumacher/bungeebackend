package me.doktormedrasen.backend.bungee.api.punishment;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

/**
 * Class by DoktorMedRasen
 */

@Getter
@Setter
@AllArgsConstructor
public class BackendLog {

    private LogType logType;
    private BackendPunishment punishment;
    private UUID executor;
    private String reason;
    private Date date;

    public static enum LogType {
        BAN,
        UNBAN,
        MUTE,
        UNMUTE;
    }

}
