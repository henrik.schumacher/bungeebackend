package me.doktormedrasen.backend.bungee.api.packets;

import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import me.doktormedrasen.backend.bungee.api.manager.BackendManager;
import me.doktormedrasen.backend.telnet.IncomingPacket;
import me.doktormedrasen.backend.telnet.PacketDataSerializer;
import me.doktormedrasen.backend.telnet.TelnetHandler;

import java.util.UUID;

/**
 * Class by DoktorMedRasen :P
 */

public class PacketPlayInKickPlayer implements IncomingPacket {

    private UUID uuid;
    private String reason;

    public void decode(PacketDataSerializer serializer) {
        this.uuid = serializer.readUUID();
        this.reason = serializer.readString();
    }

    public void handle(TelnetHandler handler) {
        BackendPlayer player = BackendManager.getInstance().getPlayer(this.uuid);
        if (player != null) {
            player.disconnect(this.reason);
        }
    }
}
