package me.doktormedrasen.backend.bungee.api.manager;

import com.mongodb.client.model.Filters;
import com.mongodb.client.model.GraphLookupOptions;
import lombok.Getter;
import me.doktormedrasen.backend.bungee.Backend;
import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import me.doktormedrasen.backend.bungee.api.packets.PacketPlayOutCustomMessage;
import me.doktormedrasen.backend.bungee.api.permissions.PermissionGroup;
import me.doktormedrasen.backend.bungee.api.permissions.PermissionPackage;
import me.doktormedrasen.backend.utils.ByteArrayDataInput;
import me.doktormedrasen.backend.utils.ByteArrayDataOutput;
import org.bson.Document;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * Class by DoktorMedRasen :P
 */

@Getter
public class PermissionManager {

    @Getter
    private static PermissionManager instance;
    private PermissionGroup defaultGroup;
    private List<PermissionGroup> groups;
    private List<PermissionPackage> packages;

    public PermissionManager() {
        instance = this;
        this.groups = new ArrayList<>();
        this.packages = new ArrayList<>();
        this.load();
        Backend.getInstance().enableModule(this);
    }

    public void disable() {
        instance = null;
        Backend.getInstance().disableModule(this);
    }

    private void load() {
        for (Document document : MongoManager.getInstance().getGroupsCollection().find()) {
            this.groups.add(PermissionGroup.load(document.getString("name")));
        }

        for  (Document document : MongoManager.getInstance().getPackagesCollection().find()) {
            this.packages.add(PermissionPackage.load(document.getString("name")));
        }

        if (PermissionGroup.load("default") == null) {
            PermissionGroup group = new PermissionGroup("default", "§7Spieler", "§7", "§7", "z99", Collections.emptyList(), Collections.emptyList(), Collections.emptyList());
            group.save();
            this.groups.add(group);
        }

        if (PermissionGroup.load("administrator") == null) {
            PermissionGroup group = new PermissionGroup("administrator", "§4Administrator", "§4Admin §f| §4", "§4", "a1", Collections.singletonList("*"), Collections.emptyList(), Collections.emptyList());
            group.save();
            this.groups.add(group);
        }
        this.defaultGroup = PermissionGroup.load("default");
    }

    public PermissionGroup parseGroup(Document document) {
        if (document != null && document.containsKey("permissions")) {
            final Document permissions = document.get("permissions", Document.class);
            if (permissions.containsKey("group")) {
                final PermissionGroup group = PermissionGroup.load(permissions.getString("group"));
                if (group != null) {
                    return group;
                }
            }
        }
        return this.getDefaultGroup();
    }

    public List<PermissionPackage> parsePackages(Document document) {
        Document permissions;
        if (document != null && document.containsKey("permissions") && (permissions = document.get("permissions", Document.class)).containsKey("packages")) {
            List<PermissionPackage> list = new ArrayList<>();
            List<String> packages = permissions.get("packages", new ArrayList<>());
            for (String string : packages) {
                PermissionPackage permissionPackage = PermissionPackage.load(string);
                if (permissionPackage == null) continue;
                list.add(permissionPackage);
            }
            return list;
        }
        return new ArrayList<>();
    }

    public List<PermissionGroup> parseInheritances(Document document) {
        Document permissions;
        if (document != null && document.containsKey("permissions") && (permissions = document.get("permissions", Document.class)).containsKey("inheritances")) {
            List<PermissionGroup> list = new ArrayList<>();
            List<String> inheritances = permissions.get("inheritances", new ArrayList<>());
            for (String string : inheritances) {
                PermissionGroup group = PermissionGroup.load(string);
                if (group == null) continue;
                list.add(group);
            }
            return list;
        }
        return new ArrayList<>();
    }

    public List<String> parsePermissions(Document document) {
        Document permissions;
        if (document != null && document.containsKey("permissions") && (permissions = document.get("permissions", Document.class)).containsKey("permissions")) {
            return permissions.get("permissions", new ArrayList<>());
        }
        return new ArrayList<>();
    }

    public Document loadGroup(String name) {
        return MongoManager.getInstance().getGroupsCollection().find(Filters.eq("name", name.toLowerCase())).first();
    }

    public Document loadPackage(String name) {
        return MongoManager.getInstance().getPackagesCollection().find(Filters.eq("name", name.toLowerCase())).first();
    }

    public void saveAndUpdateGroup(UUID uuid, PermissionGroup group) {
        MongoManager.getInstance().getPlayersCollection().updateOne(
                Filters.eq("uuid", uuid.toString()),
                new Document("$set", new Document("permissions.group", group.getName()))
        );
        this.sendPlayerUpdate(uuid);
    }

    public void saveAndUpdateInheritances(UUID uuid, List<PermissionGroup> inheritances) {
        List<String> list = new ArrayList<>();
        inheritances.forEach(group -> list.add(group.getName()));
        MongoManager.getInstance().getPlayersCollection().updateOne(
                Filters.eq("uuid", uuid.toString()),
                new Document("$set", new Document("permissions.inheritances", list))
        );
        this.sendPlayerUpdate(uuid);
    }

    public void saveAndUpdatePackages(UUID uuid, List<PermissionPackage> packages) {
        List<String> list = new ArrayList<>();
        packages.forEach(permissionPackages -> list.add(permissionPackages.getName()));
        MongoManager.getInstance().getPlayersCollection().updateOne(
                Filters.eq("uuid", uuid.toString()),
                new Document("$set", new Document("permissions.packages", list))
        );
        this.sendPlayerUpdate(uuid);
    }

    public void saveAndUpdatePermissions(UUID uuid, List<String> permissions) {
        MongoManager.getInstance().getPlayersCollection().updateOne(
                Filters.eq("uuid", uuid.toString()),
                new Document("$set", new Document("permissions.permissions", permissions))
        );
        this.sendPlayerUpdate(uuid);
    }

    public void saveAndUpdateDisplayName(PermissionGroup group, String displayName) {
        if (group == null) return;
        group.setDisplayName(displayName);
        group.save();
        this.sendGroupUpdate(group.getName());
    }

    public void saveAndUpdateTabPrefix(PermissionGroup group, String tabPrefix) {
        if (group == null) return;
        group.setTabPrefix(tabPrefix);
        group.save();
        this.sendGroupUpdate(group.getName());
    }

    public void saveAndUpdateChatPrefix(PermissionGroup group, String chatPrefix) {
        if (group == null) return;
        group.setChatPrefix(chatPrefix);
        group.save();
        this.sendGroupUpdate(group.getName());
    }

    public void saveAndUpdateSortValue(PermissionGroup group, String sortValue) {
        if (group == null) return;
        group.setSortValue(sortValue);
        group.save();
        this.sendGroupUpdate(group.getName());
    }

    public void saveAndUpdatePermissions(PermissionGroup group, List<String> permissions) {
        if (group == null) return;
        group.setPermissions(permissions);
        group.save();
        this.sendGroupUpdate(group.getName());
    }

    public void saveAndUpdateInheritances(PermissionGroup group, List<String> inheritances) {
        if (group == null) return;
        group.setInheritances(inheritances);
        group.save();
        this.sendGroupUpdate(group.getName());
    }

    public void saveAndUpdatePackages(PermissionGroup group, List<String> packages) {
        if (group == null) return;
        group.setPackages(packages);
        group.save();
        this.sendGroupUpdate(group.getName());
    }

    public void deleteGroup(PermissionGroup group, PermissionGroup alternate) {
        if (group == null) return;
        if (group.getName().equals("default")) {
            throw new RuntimeException("Cannot delete default group");
        }
        if (group.getName().equals("administrator")) {
            throw new RuntimeException("Cannot delete administrator group");
        }
        this.groups.remove(group);
        if (alternate == null || group.getName().equals(alternate.getName())) {
            alternate = this.getDefaultGroup();
        }
        for (BackendPlayer player : BackendManager.getInstance().getPlayers()) {
            if (player.getGroup().equals(group)) {
                player.setGroup(alternate);
            }
            if (!player.getInheritances().contains(group)) continue;
            List<PermissionGroup> inheritances = player.getInheritances();
            inheritances.remove(group);
            player.setInheritances(inheritances);
        }
        group.delete();
        this.sendGroupDelete(group.getName());
    }

    public void addGroupOnRecieve(PermissionGroup group) {
        this.groups.add(group);
    }

    public void deleteGroupOnRecieve(PermissionGroup group) {
        this.groups.remove(group);
    }

    public void addPackageOnRecieve(PermissionPackage permissionPackage) {
        this.packages.add(permissionPackage);
    }

    public void deletePackageOnRecieve(PermissionPackage permissionPackage) {
        this.packages.remove(permissionPackage);
    }

    public void sendPlayerUpdate(UUID uuid) {
        ByteArrayDataOutput output = new ByteArrayDataOutput();
        output.writeString("playerupdate");
        output.writeUUID(uuid);
        PacketPlayOutCustomMessage packet = new PacketPlayOutCustomMessage(new ByteArrayDataOutput().writeByte((byte) 1).toByteArray(), "permissionmanager", output.toByteArray());
        BackendManager.getInstance().sendPacket(packet);
        packet = new PacketPlayOutCustomMessage(new ByteArrayDataOutput().writeByte((byte) 6).writeUUID(uuid).toByteArray(), "permissionmanager", output.toByteArray());
        BackendManager.getInstance().sendPacket(packet);
    }

    public void sendGroupUpdate(String name) {
        ByteArrayDataOutput output = new ByteArrayDataOutput();
        output.writeString("groupupdate");
        output.writeString(name);
        PacketPlayOutCustomMessage packet = new PacketPlayOutCustomMessage(new ByteArrayDataOutput().writeByte((byte) 1).toByteArray(), "permissionmanager", output.toByteArray());
        BackendManager.getInstance().sendPacket(packet);
    }

    public void sendPackageUpdate(String name) {
        ByteArrayDataOutput output = new ByteArrayDataOutput();
        output.writeString("packageupdate");
        output.writeString(name);
        PacketPlayOutCustomMessage packet = new PacketPlayOutCustomMessage(new ByteArrayDataOutput().writeByte((byte) 0).toByteArray(), "permissionmanager", output.toByteArray());
        BackendManager.getInstance().sendPacket(packet);
    }


    public void sendGroupAdd(String name) {
        ByteArrayDataOutput output = new ByteArrayDataOutput();
        output.writeString("groupadd");
        output.writeString(name);
        PacketPlayOutCustomMessage packet = new PacketPlayOutCustomMessage(new ByteArrayDataOutput().writeByte((byte) 0).toByteArray(), "permissionmanager", output.toByteArray());
        BackendManager.getInstance().sendPacket(packet);
    }

    public void sendPackageAdd(String name) {
        ByteArrayDataOutput output = new ByteArrayDataOutput();
        output.writeString("packageadd");
        output.writeString(name);
        PacketPlayOutCustomMessage packet = new PacketPlayOutCustomMessage(new ByteArrayDataOutput().writeByte((byte) 0).toByteArray(), "permissionamanager", output.toByteArray());
        BackendManager.getInstance().sendPacket(packet);
    }

    public void sendGroupDelete(String name) {
        ByteArrayDataOutput output = new ByteArrayDataOutput();
        output.writeString("groupdelete");
        output.writeString(name);
        PacketPlayOutCustomMessage packet = new PacketPlayOutCustomMessage(new ByteArrayDataOutput().writeByte((byte) 0).toByteArray(), "permissionmanager", output.toByteArray());
        BackendManager.getInstance().sendPacket(packet);
    }

    public void sendPackageDelete(String name) {
        ByteArrayDataOutput output = new ByteArrayDataOutput();
        output.writeString("packagedelete");
        output.writeString(name);
        PacketPlayOutCustomMessage packet = new PacketPlayOutCustomMessage(new ByteArrayDataOutput().writeByte((byte) 1).toByteArray(), "permissionmanager", output.toByteArray());
        BackendManager.getInstance().sendPacket(packet);
    }

    public void onRecieve(ByteArrayDataInput input) {
        String method;
        switch (method = input.readString()) {
            case "playerupdate": {
                BackendPlayer player = BackendManager.getInstance().getPlayer(input.readUUID());
                if (player == null) break;
                player.reloadPermissions();
                break;
            }
            case "groupupdate": {
                PermissionGroup group = PermissionGroup.load(input.readString());
                if (group == null) break;
                group.reload();
                break;
            }
            case "packageupdate": {
                PermissionPackage permissionPackage = PermissionPackage.load(input.readString());
                if (permissionPackage == null) break;
                permissionPackage.reload();
                break;
            }
            case "groupadd": {
                String name = input.readString();
                PermissionGroup group = PermissionGroup.load(name);
                if (group != null) {
                    group.reload();
                    break;
                }
                Document document = this.loadGroup(name);
                if (document == null) break;
                group = new PermissionGroup(document);
                this.addGroupOnRecieve(group);
                break;
            }
            case "packageadd": {
                String name = input.readString();
                PermissionPackage permissionPackage = PermissionPackage.load(name);
                if (permissionPackage != null) {
                    permissionPackage.reload();
                    break;
                }
                Document document = this.loadPackage(name);
                if (document == null) return;
                permissionPackage = new PermissionPackage(document);
                this.addPackageOnRecieve(permissionPackage);
                break;
            }
            case "groupdelete": {
                PermissionGroup group = PermissionGroup.load(input.readString());
                if (group == null) break;
                this.deleteGroupOnRecieve(group);
                break;
            }
            case "packagedelete": {
                PermissionPackage permissionPackage = PermissionPackage.load(input.readString());
                if (permissionPackage == null) break;
                this.deletePackageOnRecieve(permissionPackage);
                break;
            }
        }
    }

}