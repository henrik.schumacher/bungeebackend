package me.doktormedrasen.backend.bungee.api.events;

import lombok.Getter;
import lombok.Setter;
import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Event;

/**
 * Class by DoktorMedRasen
 */

@Getter
public class ServerKickEvent extends Event {

    private BackendPlayer player;
    private ServerInfo kickedFrom;
    @Setter
    private BaseComponent[] kickReasonComponent;
    @Setter
    private ServerInfo cancelServer;
    @Setter
    private boolean cancelled;

    public ServerKickEvent(BackendPlayer player, ServerInfo kickedFrom, BaseComponent[] kickReasonComponent, ServerInfo cancelServer) {
        this.player = player;
        this.kickedFrom = kickedFrom;
        this.kickReasonComponent = kickReasonComponent;
        this.cancelServer = cancelServer;
        this.cancelled = false;
    }

}
