package me.doktormedrasen.backend.bungee.api.events;

import lombok.Getter;
import lombok.Setter;
import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import net.md_5.bungee.api.plugin.Event;

/**
 * Class by DoktorMedRasen
 */

@Getter
public class ChatEvent extends Event {

    private BackendPlayer player;
    @Setter
    private String message;
    @Setter
    private boolean cancelled;

    public ChatEvent(BackendPlayer player, String message) {
        this.player = player;
        this.message = message;
        this.cancelled = false;
    }

}
