package me.doktormedrasen.backend.bungee.api.punishment;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Class by DoktorMedRasen
 */

@Getter
@Setter
@AllArgsConstructor
public class BackendPunishment {

    private UUID executor;
    private String reason;
    private Date date;
    private Date until;
    private int denied;


    public boolean isPermanent() {
        return this.until.getTime() == -1;
    }

    public String getDateString() {
        return new SimpleDateFormat("dd.MM.yyyy HH:mm").format(this.date);
    }

    public String getUntilString() {
        return new SimpleDateFormat("dd.MM.yyyy HH:mm").format(this.until);
    }

    public long getDuration() {
        if (this.isPermanent()) return -1;
        return this.until.getTime() - this.date.getTime();
    }

    public long getRemaining() {
        if (this.isPermanent()) return -1;
        return this.until.getTime() - System.currentTimeMillis();
    }


    public String getDurationStringEnglish(String color) {
        if (this.isPermanent()) {
            return color + "PERMANENT";
        }
        return this.parseEnglish(this.getDuration()).replace("§r", color);
    }

    public String getDurationStringGerman(String color) {
        if (this.isPermanent()) {
            return color + "PERMANENT";
        }
        return this.parseGerman(this.getDuration()).replace("§r", color);
    }

    public String getRemainingStringEnglish(final String color) {
        if (this.isPermanent()) {
            return color + "PERMANENT";
        }
        return this.parseEnglish(this.getRemaining()).replace("§r", color);
    }

    public String getRemainingStringGerman(final String color) {
        if (this.isPermanent()) {
            return color + "PERMANENT";
        }
        return this.parseGerman(this.getRemaining()).replace("§r", color);
    }

    private String parseEnglish(long duration) {
        int days = 0;
        int hours = 0;
        int minutes = 0;
        int seconds = 0;
        while (duration >= (1000 * 60 * 60 * 24)) {
            ++days;
            duration -= 86400000L;
        }
        while (duration >= (1000 * 60 * 60)) {
            ++hours;
            duration -= (1000 * 60 * 60);
        }
        while (duration >= (1000 * 60)) {
            ++minutes;
            duration -= (1000 * 60);
        }
        while (duration >= 1000) {
            ++seconds;
            duration -= 1000;
        }
        String string = "\u00a7r";
        if (days != 0) {
            string = string + days + " days§8, §r";
        }
        if (hours != 0) {
            string = string + hours + " hours§8, §r";
        }
        if (minutes != 0) {
            string = string + minutes + " minutes§8, §r";
        }
        if (seconds != 0) {
            string = string + seconds + " seconds§8, §r";
        }
        if (string.endsWith("§8, §r")) {
            string = string.substring(0, string.length() - "§8, §r".length());
        }
        return string;
    }

    private String parseGerman(long duration) {
        return this.parseEnglish(duration).replace("days", "Tage").replace("hours", "Stunden").replace("minutes", "Minuten").replace("seconds", "Sekunden");
    }

}