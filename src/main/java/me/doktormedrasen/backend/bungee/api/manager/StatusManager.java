package me.doktormedrasen.backend.bungee.api.manager;

import com.mongodb.client.model.Filters;
import lombok.Getter;
import me.doktormedrasen.backend.bungee.Backend;
import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import org.bson.Document;

/**
 * Class by DoktorMedRasen
 */

public class StatusManager {

    @Getter
    private static StatusManager instance;

    public StatusManager() {
        instance = this;
        this.setAllOffline();
        Backend.getInstance().enableModule(this);
    }

    public void disable() {
        instance = null;
        this.setAllOffline();
        Backend.getInstance().disableModule(this);
    }

    public String parseProxy(Document document) {
        if (document != null && document.containsKey("online")) {
            Document online = document.get("online", Document.class);
            return online.getString("proxy");
        }
        return null;
    }

    public String parseServer(Document document) {
        if (document != null && document.containsKey("online")) {
            Document online = document.get("online", Document.class);
            return online.getString("server");
        }
        return null;
    }

    public void setOnline(BackendPlayer player) {
        MongoManager.getInstance().getPlayersCollection().updateOne(
                Filters.eq("uuid", player.getUniqueId().toString()),
                new Document("$set", new Document("online",  new Document("proxy", player.getProxy()).append("server", player.getProxy())))
        );
    }

    public void setServer(BackendPlayer player) {
        MongoManager.getInstance().getPlayersCollection().updateOne(
                Filters.eq("uuid", player.getUniqueId().toString()),
                new Document("$set", new Document("online.server", player.getServer()))
        );
    }

    public void setOffline(BackendPlayer player) {
        MongoManager.getInstance().getPlayersCollection().updateOne(
                Filters.eq("uuid", player.getUniqueId().toString()),
                new Document("$set", new Document("online", new Document("proxy", null).append("server", null)))
        );
    }

    public void setAllOffline() {
        MongoManager.getInstance().getPlayersCollection().updateMany(
                Filters.eq("online.proxy", Backend.getInstance().getProxyName()),
                new Document("$set", new Document("online", new Document("proxy", null).append("server", null)))
        );
    }

}
