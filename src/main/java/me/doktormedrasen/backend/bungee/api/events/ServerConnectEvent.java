package me.doktormedrasen.backend.bungee.api.events;

import lombok.Getter;
import lombok.Setter;
import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Event;

/**
 * Class by DoktorMedRasen
 */

@Getter
public class ServerConnectEvent extends Event {

    private BackendPlayer player;
    @Setter
    private ServerInfo target;
    @Setter
    private boolean cancelled;

    public ServerConnectEvent(BackendPlayer player, ServerInfo target) {
        this.player = player;
        this.target = target;
        this.cancelled = false;
    }

}
