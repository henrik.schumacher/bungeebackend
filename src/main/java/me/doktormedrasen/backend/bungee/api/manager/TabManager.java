package me.doktormedrasen.backend.bungee.api.manager;

import com.mongodb.client.model.Filters;
import lombok.Getter;
import me.doktormedrasen.backend.bungee.Backend;
import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import me.doktormedrasen.backend.bungee.api.packets.PacketPlayOutCustomMessage;
import me.doktormedrasen.backend.utils.ByteArrayDataInput;
import me.doktormedrasen.backend.utils.ByteArrayDataOutput;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Class by DoktorMedRasen
 */

@Getter
public class TabManager {

    @Getter
    private static TabManager instance;
    private List<String> tabFooters;
    private int current;
    private ScheduledTask task;

    public TabManager() {
        instance = this;
        this.load();
        this.task = ProxyServer.getInstance().getScheduler().schedule(Backend.getInstance(), () -> {
            ++this.current;
            if (this.current >= this.tabFooters.size()) {
                this.current = 0;
            }
            for (BackendPlayer player : BackendManager.getInstance().getPlayers()) {
                if (!player.isLocal()) continue;
                this.applyTabHeaderFooter(player);
            }
        }, 10, 10, TimeUnit.SECONDS);
        Backend.getInstance().enableModule(this);
    }

    public void disable() {
        instance = null;
        Backend.getInstance().disableModule(this);
    }

    private void load() {
        this.tabFooters = new ArrayList<>();
        for (Document document : MongoManager.getInstance().getTabmanagerCollection().find()) {
            this.tabFooters.add(document.getString("footer"));
        }
    }

    public void applyTabHeaderFooter(BackendPlayer player) {
        if (player.getConnection() == null || player.getConnection().getServer() == null || player.getConnection().getServer().getInfo() == null) {
            return;
        }
        String spacer = "";
        TextComponent header = new TextComponent("§a" + player.getConnection().getServer().getInfo().getName().split("-")[0] + "\n" + player.language("§fDoktorMedRasen §9Netzwerk", "§fDoktorMedRasen §9Network") + "\n" + spacer);
        TextComponent footer = new TextComponent("\n" + (this.tabFooters.isEmpty() ? "" : new StringBuilder().append("  ").append(this.tabFooters.get(this.current)).append(" \n").toString()) + (LoginManager.getInstance().isMaintenance() ? player.language("§cWartungsarbeiten", "§cMaintenance") : ""));
        player.getConnection().setTabHeader(header, footer);
    }

    public void addAndUpdateTabFooter(String footer) {
        this.tabFooters.add(footer);
        MongoManager.getInstance().getTabmanagerCollection().insertOne(new Document("footer", footer));
        this.sendUpdate();
    }

    public void deleteAndUpdateTabFooter(String footer) {
        this.tabFooters.remove(footer);
        MongoManager.getInstance().getTabmanagerCollection().deleteOne(Filters.eq("footer", footer));
        this.sendUpdate();
    }

    public void sendUpdate() {
        PacketPlayOutCustomMessage packet = new PacketPlayOutCustomMessage(new ByteArrayDataOutput().writeByte((byte)1).toByteArray(), "tabmanager", new ByteArrayDataOutput().writeByte((byte)0).toByteArray());
        BackendManager.getInstance().sendPacket(packet);
    }

    public void onRecieve(ByteArrayDataInput input) {
        if (input.readByte() == 0) {
            this.load();
        }
    }

}