package me.doktormedrasen.backend.bungee.api;

import lombok.Getter;
import me.doktormedrasen.backend.bungee.api.manager.BackendManager;
import me.doktormedrasen.backend.bungee.api.manager.PermissionManager;
import me.doktormedrasen.backend.bungee.api.message.BackendComponent;
import me.doktormedrasen.backend.bungee.api.permissions.PermissionGroup;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;

import java.util.UUID;

/**
 * Class by DoktorMedRasen
 */

public class BackendConsole implements BackendSender {

    private String name = "CONSOLE";
    private UUID uniqueId = BackendManager.getInstance().getDefaultUUID();
    @Getter
    private CommandSender console;

    public BackendConsole(CommandSender sender) {
        this.console = sender;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public UUID getUniqueId() {
        return this.uniqueId;
    }

    @Override
    public String getDisplayName() {
        PermissionGroup group = PermissionGroup.load("administrator");
        if (group != null) {
            return group.getChatPrefix() + this.name;
        }
        return PermissionManager.getInstance().getDefaultGroup().getChatPrefix() + this.name;
    }

    @Override
    public void sendMessage(BaseComponent component) {
        this.console.sendMessage(component);
    }

    @Override
    public void sendMessage(String message) {
        this.sendMessage(new TextComponent(message));
    }

    @Override
    public void sendMessage(BaseComponent german, BaseComponent english) {
        this.sendMessage(english);
    }

    @Override
    public void sendMessage(String german, String english) {
        this.sendMessage(new TextComponent(german), new TextComponent(english));
    }

    @Override
    public void sendMessage(BackendComponent message) {
        this.sendMessage(message.getComponent());
    }

    @Override
    public void sendMessage(BackendComponent german, BackendComponent english) {
        this.sendMessage(german.getComponent(), english.getComponent());
    }

    @Override
    public String language(String english, String german) {
        return english;
    }

    @Override
    public boolean hasPermission(String permission) {
        return true;
    }

}