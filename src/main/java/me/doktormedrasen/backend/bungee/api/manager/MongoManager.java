package me.doktormedrasen.backend.bungee.api.manager;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.Getter;
import me.doktormedrasen.backend.bungee.Backend;
import org.bson.Document;

/**
 * Class by DoktorMedRasen :P
 */

@Getter
public class MongoManager {

    @Getter
    private static MongoManager instance;
    private MongoClient mongoClient;
    private MongoDatabase networkDatabase;
    private MongoCollection<Document> playersCollection;
    private MongoCollection<Document> groupsCollection;
    private MongoCollection<Document> packagesCollection;
    private MongoCollection<Document> punishmentsCollection;
    private MongoCollection<Document> tabmanagerCollection;
    private MongoCollection<Document> pingManagerCollection;
    private MongoCollection<Document> loginsCollection;
    private MongoCollection<Document> maintenanceCollection;
    private MongoCollection<Document> hostsCollection;

    public MongoManager() {
        instance = this;
        this.mongoClient = new MongoClient(new MongoClientURI("mongodb+srv://DoktorMedRasen:Jugendfeuerwehr_112@reallyfedatenbank-nz5ib.mongodb.net"));
        this.networkDatabase = this.mongoClient.getDatabase("network");
        this.playersCollection = this.networkDatabase.getCollection("players");
        this.groupsCollection = this.networkDatabase.getCollection("groups");
        this.packagesCollection = this.networkDatabase.getCollection("packages");
        this.punishmentsCollection = this.networkDatabase.getCollection("punishments");
        this.tabmanagerCollection = this.networkDatabase.getCollection("tabmanager");
        this.pingManagerCollection = this.networkDatabase.getCollection("pingmanager");
        this.loginsCollection = this.networkDatabase.getCollection("logins");
        this.maintenanceCollection = this.networkDatabase.getCollection("maintenance");
        this.hostsCollection = this.networkDatabase.getCollection("hosts");
        Backend.getInstance().enableModule(this);
    }

    public void disable() {
        instance = null;
        this.mongoClient.close();
        Backend.getInstance().disableModule(this);
    }

    public MongoDatabase getMongoDatabase(String name) {
        return this.mongoClient.getDatabase(name);
    }

    public MongoCollection<Document> getMongoCollection(String name) {
        return this.networkDatabase.getCollection(name);
    }

}
