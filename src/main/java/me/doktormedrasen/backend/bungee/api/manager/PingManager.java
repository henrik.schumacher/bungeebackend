package me.doktormedrasen.backend.bungee.api.manager;

import com.mongodb.client.model.Filters;
import lombok.Getter;
import lombok.Setter;
import me.doktormedrasen.backend.bungee.Backend;
import me.doktormedrasen.backend.bungee.api.packets.PacketPlayOutCustomMessage;
import me.doktormedrasen.backend.bungee.api.packets.PacketPlayOutSlots;
import me.doktormedrasen.backend.utils.ByteArrayDataInput;
import me.doktormedrasen.backend.utils.ByteArrayDataOutput;
import org.bson.Document;

/**
 * Class by DoktorMedRasen
 */

@Getter
public class PingManager {

    @Getter
    private static PingManager instance;
    private String motd;
    private String hovermotd;
    private int slots;

    public PingManager() {
        instance = this;
        this.load();
        Backend.getInstance().enableModule(this);
    }

    public void disable() {
        instance = null;
        Backend.getInstance().disableModule(this);
    }

    private void load() {
        Document document = MongoManager.getInstance().getPingManagerCollection().find().first();
        if (document == null) {
            MongoManager.getInstance().getPingManagerCollection().insertOne(new Document("motd", "MOTD").append("hovermotd", "HOVERMOTD").append("slots", 1));
            this.motd = "MOTD";
            this.hovermotd = "HOVERMOTD";
            this.slots = 1;
        } else {
            this.motd = document.getString("motd");
            this.hovermotd = document.getString("hovermotd");
            this.slots = document.getInteger("slots");
        }
    }

    public void saveAndUpdateMOTD(String motd) {
        this.motd = motd;
        MongoManager.getInstance().getPingManagerCollection().updateOne(
                Filters.exists("motd"),
                new Document("$set", new Document("motd", motd))
        );
        this.sendUpdate();
    }

    public void saveAndUodateHoverMOTD(String hovermotd) {
        this.hovermotd = hovermotd;
        MongoManager.getInstance().getPingManagerCollection().updateOne(
                Filters.exists("hovermotd"),
                new Document("$set", new Document("hovermotd", hovermotd))
        );
        this.sendUpdate();
    }

    public void saveAndUpdateSlots(int slots) {
        this.slots = slots;
        MongoManager.getInstance().getPingManagerCollection().updateOne(
                Filters.exists("slots"),
                new Document("$set", new Document("slots", slots))
        );
        this.sendUpdate();
    }

    public void sendUpdate() {
        PacketPlayOutCustomMessage packet = new PacketPlayOutCustomMessage(new ByteArrayDataOutput().writeByte((byte) 1).toByteArray(), "pingmanager", new ByteArrayDataOutput().writeString("update").toByteArray());
        BackendManager.getInstance().sendPacket(packet);
        BackendManager.getInstance().sendPacket(new PacketPlayOutSlots(this.slots));
    }

    public void onRecieve(ByteArrayDataInput input) {
        String method = input.readString();
        if (method.equals("update")) {
            this.load();
        }
    }

}
