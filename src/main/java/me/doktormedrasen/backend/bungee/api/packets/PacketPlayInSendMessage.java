package me.doktormedrasen.backend.bungee.api.packets;

import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import me.doktormedrasen.backend.bungee.api.manager.BackendManager;
import me.doktormedrasen.backend.telnet.IncomingPacket;
import me.doktormedrasen.backend.telnet.PacketDataSerializer;
import me.doktormedrasen.backend.telnet.TelnetHandler;
import net.md_5.bungee.chat.ComponentSerializer;

import java.util.UUID;

/**
 * Class by DoktorMedRasen :P
 */

public class PacketPlayInSendMessage implements IncomingPacket {

    private UUID uuid;
    private String message;

    public void decode(PacketDataSerializer serializer) {
        this.uuid = serializer.readUUID();
        this.message = serializer.readString();
    }

    public void handle(TelnetHandler handler) {
        BackendPlayer player = BackendManager.getInstance().getPlayer(this.uuid);
        if (player != null) {
            player.sendMessage(ComponentSerializer.parse(this.message)[0]);
        }
    }

}
