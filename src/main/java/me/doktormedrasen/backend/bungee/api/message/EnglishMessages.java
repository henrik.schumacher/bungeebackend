package me.doktormedrasen.backend.bungee.api.message;

import me.doktormedrasen.backend.bungee.Backend;

import java.util.UUID;

/**
 * Class by DoktorMedRasen
 */

public class EnglishMessages {

    public static final String NOPERMISSIONS = Backend.PREFIX + "§cYou §cdon't §chave §cpermissions §cto §cexecute §cthis §ccommand";
    public static final String MAINTENANCE_KICK = "\n§cThe network is now under maintenance";
    public static final String SERVER_FULL = "§cThe server is full";
    public static final String PUNISHMENT_BYPASS = "§cIt seems you are trying to bypass a punishment with an alternative account\n\n§cContact a staff member if you believe this is an error";

    public static final String UUIDNOTFOUND(UUID uuid) {
        return UUIDNOTFOUND(uuid.toString());
    }

    public static final String UUIDNOTFOUND(String uuid) {
        return Backend.PREFIX + "UUID of §8'§f" + uuid + "§8' §7could §cnot §7be found";
    }

    public static final String MAINTENANCE_FORBIDDEN(String reason) {
        return "§cThe network is under maintenance\n§cReason§8: §f" + reason;
    }

    public static final String BANNED(String reason, String remaining) {
        return "§cYou've been banned from the network\n\n§fReason§8: §c" + reason + "\n§fRemaining time§8: §c" + remaining;
    }
}