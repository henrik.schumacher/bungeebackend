package me.doktormedrasen.backend.bungee.api.message;

import me.doktormedrasen.backend.bungee.Backend;

import java.util.UUID;

/**
 * Class by DoktorMedRasen
 */

public class GermanMessages {

    public static final String NOPERMISSIONS = Backend.PREFIX + "§cDu §chast §ckeine §cBerechtigungen §cum §cdiesen §cBefehl §causzuführen";
    public static final String MAINTENANCE_KICK = "\n§cDas Netzwerk befindet sich jetzt in Wartungsarbeiten";
    public static final String SERVER_FULL = "§cDer Server ist voll";
    public static final String PUNISHMENT_BYPASS = "§cEs scheint, als würdest du versuchen einen Bann zu umgehen\n\n§cMelde dich beim einem Teammitglied, sollte dies ein Fehler sein";

    public static final String UUIDNOTFOUND(UUID uuid) {
        return UUIDNOTFOUND(uuid.toString());
    }

    public static final String UUIDNOTFOUND(String uuid) {
        return Backend.PREFIX + "UUID von §8'§f" + uuid + "§8' §7konnte §cnicht §7gefunden werden";
    }

    public static final String MAINTENANCE_FORBIDDEN(String reason) {
        return "§cDas Netzwerk befindet sich in Wartungsarbeiten\n§cGrund§8: §f" + reason;
    }

    public static final String BANNED(String reason, String remaining) {
        return "§cDu wurdest vom Netzwerk gebannt\n\n§fGrund§8: §c" + reason + "\n§fVerbleibende Zeit§8: §c" + remaining;
    }
}