package me.doktormedrasen.backend.bungee;

import lombok.Getter;
import lombok.Setter;
import me.doktormedrasen.backend.bungee.api.manager.*;
import me.doktormedrasen.backend.bungee.commands.TestCommand;
import me.doktormedrasen.backend.bungee.listener.*;
import me.doktormedrasen.backend.bungee.pluginmanager.PluginManagerCommand;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class by DoktorMedRasen :P
 */

@Getter
public class Backend extends Plugin {

    @Getter
    private static Backend instance;
    public static final String PREFIX = "§cSystem §f| §7";
    private String proxyName;
    private File file;
    private Configuration configuration;
    @Setter
    private boolean debugEnabled;

    @Override
    public void onLoad() {
        instance = this;

        this.file = new File("settings/Backend.yml");
        if (!this.file.exists()) {
            try {
                if (!this.file.getParentFile().exists()) {
                    this.file.getParentFile().mkdir();
                }
                this.file.createNewFile();
                this.configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(this.file);
                this.configuration.set("name", "BungeeCord");
                this.configuration.set("database.host","localhost");
                this.configuration.set("database.port", 27017);
                this.configuration.set("database.user", "root");
                this.configuration.set("database.password", "password");
                this.configuration.set("manager.host", "host");
                this.configuration.set("manager.port", 23);
                ConfigurationProvider.getProvider(YamlConfiguration.class).save(this.configuration, this.file);
                System.out.println("Please configure the properties in 'settings/Backend.yml'");
                System.exit(0);
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(0);
            }
        } else {
            try {
                this.configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(this.file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.proxyName = this.configuration.getString("name");
        this.debugEnabled = false;
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
    }

    @Override
    public void onEnable() {
        new MongoManager();
        new BackendManager();
        new PingManager();
        new PermissionManager();
        new TabManager();
        new LanguageManager();
        new PunishmentManager();
        new StatusManager();
        new LoginManager();
        new ConnectManager();
        BackendManager.getInstance().connect();
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new PluginManagerCommand());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new TestCommand());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new ChatListener());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new LoginListener());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new PermissionCheckListener());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new PlayerDisconnectListener());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new PostLoginListener());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new ProxyPingListener());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new ServerConnectedListener());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new ServerConnectListener());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new ServerDisconnectListener());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new ServerKickListener());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new ServerSwitchListener());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new TabCompleteListener());
    }

    @Override
    public void onDisable() {
        BackendManager.getInstance().disable();
        PingManager.getInstance().disable();
        PermissionManager.getInstance().disable();
        TabManager.getInstance().disable();
        LanguageManager.getInstance().disable();
        PunishmentManager.getInstance().disable();
        StatusManager.getInstance().disable();
        LoginManager.getInstance().disable();
        ConnectManager.getInstance().disable();
        MongoManager.getInstance().disable();
    }

    public void debug(String message)  {
        if (this.debugEnabled) {
            ProxyServer.getInstance().getConsole().sendMessage(new TextComponent(message));
        }
    }

    public void enableModule(Object object) {
        ProxyServer.getInstance().getConsole().sendMessage(new TextComponent("Backend - Module " + object.getClass().getSimpleName() + " is now §aenabled"));
    }

    public void disableModule(Object object) {
        ProxyServer.getInstance().getConsole().sendMessage(new TextComponent("Backend - Module " + object.getClass().getSimpleName() + " is now §cdisabled"));
    }

}