package me.doktormedrasen.backend.bungee.commands;

import me.doktormedrasen.backend.bungee.api.BackendCommand;
import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import me.doktormedrasen.backend.bungee.api.BackendSender;

/**
 * Class by DoktorMedRasen
 */

public class TestCommand extends BackendCommand {

    public TestCommand() {
        super("test");
    }

    @Override
    public void execute(BackendSender sender, String[] args) {
        if (sender instanceof BackendPlayer) {
            BackendPlayer player = (BackendPlayer) sender;
            player.sendMessage(player.getGroup().getDisplayName());
            player.sendMessage(player.getGroup().getPermissions().toString());
            player.sendMessage(player.getPermissions().toString());
        }
    }

    @Override
    public Iterable<String> onTabComplete(BackendSender var1, String[] var2) {
        return null;
    }
}
