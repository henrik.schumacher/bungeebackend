package me.doktormedrasen.backend.bungee.listener;

import me.doktormedrasen.backend.bungee.api.manager.BackendManager;
import me.doktormedrasen.backend.bungee.api.manager.LoginManager;
import me.doktormedrasen.backend.bungee.api.manager.PingManager;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Class by DoktorMedRasen
 */

public class ProxyPingListener implements Listener {

    @EventHandler
    public void onProxyPing(ProxyPingEvent e) {
        ServerPing ping = e.getResponse();
        ping.setDescriptionComponent(new TextComponent(PingManager.getInstance().getHovermotd() + "§r\n" + PingManager.getInstance().getMotd()));
        ping.setPlayers(new ServerPing.Players(PingManager.getInstance().getSlots(), BackendManager.getInstance().getPlayers().size(), null));
        if (LoginManager.getInstance().isMaintenance()) {
            ping.setVersion(new ServerPing.Protocol("§cMaintenance", 1));
            ping.setPlayers(new ServerPing.Players(1, BackendManager.getInstance().getPlayers().size(), null));
        }
        e.setResponse(ping);
    }

}
