package me.doktormedrasen.backend.bungee.listener;

import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import me.doktormedrasen.backend.bungee.api.manager.BackendManager;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.event.PermissionCheckEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Class by DoktorMedRasen
 */

public class PermissionCheckListener implements Listener {

    @EventHandler
    public void onPermissionCheck(PermissionCheckEvent e) {
        if (e.getSender().equals(ProxyServer.getInstance().getConsole())) {
            e.setHasPermission(true);
            return;
        }
        String permission = e.getPermission().toLowerCase();
        BackendPlayer player = BackendManager.getInstance().getPlayer(e.getSender().getName());
        if (player == null) {
            e.setHasPermission(e.getSender().getPermissions().contains(permission));
            return;
        }
        e.setHasPermission(player.hasPermission(e.getPermission()));
    }

}
