package me.doktormedrasen.backend.bungee.listener;

import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import me.doktormedrasen.backend.bungee.api.manager.BackendManager;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Class by DoktorMedRasen
 */

public class ServerConnectedListener implements Listener {

    @EventHandler
    public void onServerConnected(ServerConnectedEvent e) {
        BackendPlayer player = BackendManager.getInstance().getPlayer(e.getPlayer().getUniqueId());
        me.doktormedrasen.backend.bungee.api.events.ServerConnectedEvent event = new me.doktormedrasen.backend.bungee.api.events.ServerConnectedEvent(player, e.getServer());
        ProxyServer.getInstance().getPluginManager().callEvent(event);
    }

}
