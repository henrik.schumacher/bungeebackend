package me.doktormedrasen.backend.bungee.listener;

import me.doktormedrasen.backend.bungee.Backend;
import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import me.doktormedrasen.backend.bungee.api.manager.BackendManager;
import me.doktormedrasen.backend.bungee.api.manager.StatusManager;
import me.doktormedrasen.backend.bungee.api.manager.TabManager;
import me.doktormedrasen.backend.bungee.api.packets.PacketPlayOutServerSwitch;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Class by DoktorMedRasen
 */

public class ServerSwitchListener implements Listener {

    @EventHandler
    public void onServerSwitch(ServerSwitchEvent e) {
        BackendPlayer player = BackendManager.getInstance().getPlayer(e.getPlayer().getUniqueId());
        if (player == null) {
            return;
        }
        player.setServer(e.getPlayer().getServer().getInfo().getName());
        ProxyServer.getInstance().getPluginManager().callEvent(new me.doktormedrasen.backend.bungee.api.events.ServerSwitchEvent(player));
        BackendManager.getInstance().sendPacket(new PacketPlayOutServerSwitch(player.getUniqueId(), player.getServer()));
        TabManager.getInstance().applyTabHeaderFooter(player);
        ProxyServer.getInstance().getScheduler().runAsync(Backend.getInstance(), () -> StatusManager.getInstance().setServer(player));
    }

}
