package me.doktormedrasen.backend.bungee.listener;

import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import me.doktormedrasen.backend.bungee.api.manager.BackendManager;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.event.ServerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Class by DoktorMedRasen
 */

public class ServerDisconnectListener implements Listener {

    @EventHandler
    public void onServerDisconnect(ServerDisconnectEvent e) {
        BackendPlayer player = BackendManager.getInstance().getPlayer(e.getPlayer().getUniqueId());
        if (player == null) {
            return;
        }
        me.doktormedrasen.backend.bungee.api.events.ServerDisconnectEvent event = new me.doktormedrasen.backend.bungee.api.events.ServerDisconnectEvent(player, e.getTarget());
        ProxyServer.getInstance().getPluginManager().callEvent(event);
    }

}
