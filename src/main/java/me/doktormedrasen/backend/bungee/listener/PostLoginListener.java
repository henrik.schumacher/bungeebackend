package me.doktormedrasen.backend.bungee.listener;

import me.doktormedrasen.backend.bungee.api.manager.ConnectManager;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Class by DoktorMedRasen
 */

public class PostLoginListener implements Listener {

    @EventHandler
    public void onPostLogin(PostLoginEvent e) {
        ConnectManager.getInstance().onPostLogin(e);
    }

}
