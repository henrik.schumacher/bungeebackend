package me.doktormedrasen.backend.bungee.listener;

import me.doktormedrasen.backend.bungee.Backend;
import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import me.doktormedrasen.backend.bungee.api.manager.BackendManager;
import me.doktormedrasen.backend.bungee.api.manager.PunishmentManager;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Class by DoktorMedRasen
 */

public class ChatListener implements Listener {

    @EventHandler
    public void onChat(ChatEvent e) {
        BackendPlayer player = BackendManager.getInstance().getPlayer(((ProxiedPlayer)e.getSender()).getUniqueId());
        if (player == null) {
            e.setCancelled(true);
            return;
        }
        me.doktormedrasen.backend.bungee.api.events.ChatEvent event = new me.doktormedrasen.backend.bungee.api.events.ChatEvent(player, e.getMessage());
        ProxyServer.getInstance().getPluginManager().callEvent(event);
        if (!event.isCancelled() && !event.getMessage().startsWith("/") && player.isMuted()) {
            event.setCancelled(true);
            player.getMute().setDenied(player.getMute().getDenied() + 1);
            PunishmentManager.getInstance().saveAndUpdateMute(player.getUniqueId(), player.getMute());
            player.sendMessage(
                    Backend.PREFIX + "§cDu wurdest aus dem Chat gebannt",
                    Backend.PREFIX + "§cYou've been muted from the chat"
            );
            player.sendMessage(
                    Backend.PREFIX + "Grund§8: §c" + player.getMute().getReason(),
                    Backend.PREFIX + "Reason§8: §c" + player.getMute().getReason()
            );
            player.sendMessage(
                    Backend.PREFIX + "Verbleibende Zeit§8: §c" + player.getMute().getRemainingStringGerman("§c"),
                    Backend.PREFIX + "Remaining time§8: §c" + player.getMute().getRemainingStringEnglish("§c")
            );
            if (player.getMute().getWrittenMessage() != null) {
                player.sendMessage(
                        Backend.PREFIX + "Geschriebene Nachricht§8: §c" + player.getMute().getWrittenMessage(),
                        Backend.PREFIX + "Written message§8: §c" + player.getMute().getWrittenMessage()
                );
            }
        }
        e.setMessage(event.getMessage());
        event.setCancelled(event.isCancelled());
    }

}