package me.doktormedrasen.backend.bungee.listener;

import me.doktormedrasen.backend.bungee.api.manager.ConnectManager;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Class by DoktorMedRasen
 */

public class LoginListener implements Listener {

    @EventHandler(priority = -64)
    public void onLogin(LoginEvent event) {
        ConnectManager.getInstance().onLogin(event);
    }

}
