package me.doktormedrasen.backend.bungee.listener;

import me.doktormedrasen.backend.bungee.api.manager.ConnectManager;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Class by DoktorMedRasen
 */

public class PlayerDisconnectListener implements Listener {

    @EventHandler
    public void onPlayerDisconnect(PlayerDisconnectEvent e) {
        ConnectManager.getInstance().onDisconnect(e);
    }

}
