package me.doktormedrasen.backend.bungee.listener;

import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import me.doktormedrasen.backend.bungee.api.manager.BackendManager;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Class by DoktorMedRasen
 */

public class ServerConnectListener implements Listener {

    @EventHandler
    public void onServerConnect(ServerConnectEvent e) {
        BackendPlayer player = BackendManager.getInstance().getPlayer(e.getPlayer().getUniqueId());
        me.doktormedrasen.backend.bungee.api.events.ServerConnectEvent event = new me.doktormedrasen.backend.bungee.api.events.ServerConnectEvent(player, e.getTarget());
        ProxyServer.getInstance().getPluginManager().callEvent(event);
        e.setCancelled(event.isCancelled());
        e.setTarget(event.getTarget());
    }

}
