package me.doktormedrasen.backend.bungee.listener;

import me.doktormedrasen.backend.bungee.api.BackendPlayer;
import me.doktormedrasen.backend.bungee.api.manager.BackendManager;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Class by DoktorMedRasen
 */

public class ServerKickListener implements Listener {

    @EventHandler
    public void onServerKick(ServerKickEvent e) {
        BackendPlayer player = BackendManager.getInstance().getPlayer(e.getPlayer().getUniqueId());
        if (player == null) {
            return;
        }
        me.doktormedrasen.backend.bungee.api.events.ServerKickEvent event = new me.doktormedrasen.backend.bungee.api.events.ServerKickEvent(player, e.getKickedFrom(), e.getKickReasonComponent(), e.getCancelServer());
        ProxyServer.getInstance().getPluginManager().callEvent(event);
        e.setCancelled(event.isCancelled());
        e.setCancelServer(event.getCancelServer());
        e.setKickReasonComponent(event.getKickReasonComponent());
    }

}
