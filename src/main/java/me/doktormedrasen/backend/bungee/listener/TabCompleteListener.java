package me.doktormedrasen.backend.bungee.listener;

import net.md_5.bungee.api.event.TabCompleteEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Class by DoktorMedRasen
 */

public class TabCompleteListener implements Listener {

    @EventHandler
    public void onTabComplete(TabCompleteEvent e) {
        if (e.getCursor().equals("/")) {
            e.setCancelled(true);
        }
    }

}
