package me.doktormedrasen.backend.bungee.pluginmanager;

import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.event.AsyncEvent;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class by DoktorMedRasen :P
 */

public class AsyncEventsListener implements Listener {

    static Set<AsyncEvent<?>> uncompletedEvents;

    static void completeIntents(final Plugin plugin) {
        for (final AsyncEvent<?> event : uncompletedEvents) {
            try {
                event.completeIntent(plugin);
            }
            catch (Exception ex) {}
        }
    }

    @EventHandler
    public void onLogin(final LoginEvent event) {
        replaceCallback(event);
        rememberEvent(event);
    }

    @EventHandler
    public void onPreLogin(final PreLoginEvent event) {
        replaceCallback(event);
        rememberEvent(event);
    }

    @EventHandler
    public void onPing(final ProxyPingEvent event) {
        replaceCallback(event);
        rememberEvent(event);
    }

    private static void replaceCallback(final AsyncEvent<?> event) {
        final Callback<AsyncEvent<?>> realCallback = ReflectionUtils.getFieldValue(event, "done");
        ReflectionUtils.setFieldValue(event, "done", new WrappedCallback(realCallback));
    }

    private static void rememberEvent(final AsyncEvent<?> event) {
        uncompletedEvents.add(event);
    }

    static {
        uncompletedEvents = Collections.newSetFromMap(new ConcurrentHashMap<>());
    }

    private static class WrappedCallback implements Callback<AsyncEvent<?>>
    {
        private Callback<AsyncEvent<?>> realCallback;

        WrappedCallback(final Callback<AsyncEvent<?>> realCallback) {
            this.realCallback = realCallback;
        }

        public void done(final AsyncEvent<?> event, final Throwable throwable) {
            uncompletedEvents.remove(event);
            this.realCallback.done(event, throwable);
        }
    }

}