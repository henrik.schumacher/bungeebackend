package me.doktormedrasen.backend.bungee.pluginmanager;

import com.google.common.collect.Multimap;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.*;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Handler;
import java.util.logging.Level;

/**
 * Class by DoktorMedRasen :P
 */

public class PluginUtils {

    public static boolean loadPlugin(final File pluginfile) {
        try (final JarFile jar = new JarFile(pluginfile)) {
            JarEntry entry = jar.getJarEntry("bungee.yml");
            if (entry == null) {
                entry = jar.getJarEntry("plugin.yml");
            }
            try (final InputStream inputStream = jar.getInputStream(entry)) {
                final PluginDescription description = new Yaml().loadAs(inputStream, PluginDescription.class);
                description.setFile(pluginfile);
                final HashSet<String> plugins = new HashSet<>();
                for (final Plugin plugin : ProxyServer.getInstance().getPluginManager().getPlugins()) {
                    plugins.add(plugin.getDescription().getName());
                }
                for (final String dependency : description.getDepends()) {
                    if (!plugins.contains(dependency)) {
                        ProxyServer.getInstance().getLogger().log(Level.WARNING, "{0} (required by {1}) is unavailable", new Object[] { dependency, description.getName() });
                        return false;
                    }
                }
                final URLClassLoader loader = new PluginClassloader(new URL[] { pluginfile.toURI().toURL() });
                final Class<?> mainclass = loader.loadClass(description.getName());
                final Plugin plugin2 = (Plugin)mainclass.getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
                ReflectionUtils.invokeMethod(plugin2, "init", ProxyServer.getInstance(), description);
                final Map<String, Plugin> pluginMap = ReflectionUtils.getFieldValue(ProxyServer.getInstance().getPluginManager(), "plugins");
                pluginMap.put(description.getName(), plugin2);
                plugin2.onLoad();
                plugin2.onEnable();
                return true;
            }
        } catch (Exception e) {
            severe("Failed to load plugin", e, pluginfile.getName());
            return false;
        }
    }

    public static void unloadPlugin(final Plugin plugin) {
        final PluginManager pluginmanager = ProxyServer.getInstance().getPluginManager();
        final ClassLoader pluginclassloader = plugin.getClass().getClassLoader();
        try {
            plugin.onDisable();
            for (final Handler handler : plugin.getLogger().getHandlers()) {
                handler.close();
            }
        } catch (Throwable t) {
            severe("Exception disabling plugin", t, plugin.getDescription().getName());
        }
        pluginmanager.unregisterListeners(plugin);
        pluginmanager.unregisterCommands(plugin);
        ProxyServer.getInstance().getScheduler().cancel(plugin);
        plugin.getExecutorService().shutdownNow();
        for (final Thread thread : Thread.getAllStackTraces().keySet()) {
            if (thread.getClass().getClassLoader() == pluginclassloader) {
                try {
                    thread.interrupt();
                    thread.join(2000);
                    if (!thread.isAlive()) {
                        continue;
                    }
                    thread.stop();
                } catch (Throwable t2) {
                    severe("Failed to stop thread that belong to plugin", t2, plugin.getDescription().getName());
                }
            }
        }
        AsyncEventsListener.completeIntents(plugin);
        try {
            final Map<String, Command> commandMap = ReflectionUtils.getFieldValue(pluginmanager, "commandMap");
            commandMap.entrySet().removeIf(entry -> entry.getValue().getClass().getClassLoader() == pluginclassloader);
        } catch (Throwable t) {
            severe("Failed to cleanup commandMap", t, plugin.getDescription().getName());
        }
        try {
            final Map<String, Plugin> pluginsMap = ReflectionUtils.getFieldValue(pluginmanager, "plugins");
            pluginsMap.values().remove(plugin);
            final Multimap<Plugin, Command> commands = ReflectionUtils.getFieldValue(pluginmanager, "commandsByPlugin");
            commands.removeAll(plugin);
            final Multimap<Plugin, Listener> listeners = ReflectionUtils.getFieldValue(pluginmanager, "listenersByPlugin");
            listeners.removeAll(plugin);
        } catch (Throwable t) {
            severe("Failed to cleanup bungee internal maps from plugin refs", t, plugin.getDescription().getName());
        }
        if (pluginclassloader instanceof URLClassLoader) {
            try {
                ((URLClassLoader)pluginclassloader).close();
            }
            catch (Throwable t) {
                severe("Failed to close the classloader for plugin", t, plugin.getDescription().getName());
            }
        }
        final Set<PluginClassloader> allLoaders = ReflectionUtils.getStaticFieldValue(PluginClassloader.class, "allLoaders");
        allLoaders.remove(pluginclassloader);
    }

    private static void severe(final String message, final Throwable t, final String pluginname) {
        ProxyServer.getInstance().getLogger().log(Level.SEVERE, message + " " + pluginname, t);
    }

}
