package me.doktormedrasen.backend.bungee.pluginmanager;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Class by DoktorMedRasen :P
 */

public class ReflectionUtils {

    static <T> T getFieldValue(final Object obj, final String fieldname) {
        Class<?> clazz = obj.getClass();
        while (true) {
            try {
                final Field field = clazz.getDeclaredField(fieldname);
                field.setAccessible(true);
                return (T)field.get(obj);
            } catch (Throwable t) {
                if ((clazz = clazz.getSuperclass()) == null) {
                    return null;
                }
                continue;
            }
        }
    }

    static void setFieldValue(final Object obj, final String fieldname, final Object value) {
        Class<?> clazz = obj.getClass();
        do {
            try {
                final Field field = clazz.getDeclaredField(fieldname);
                field.setAccessible(true);
                field.set(obj, value);
            }
            catch (Throwable t) {}
        } while ((clazz = clazz.getSuperclass()) != null);
    }

    static <T> T getStaticFieldValue(Class<?> clazz, final String fieldname) {
        while (true) {
            try {
                final Field field = clazz.getDeclaredField(fieldname);
                field.setAccessible(true);
                return (T)field.get(null);
            }
            catch (Throwable t) {
                if ((clazz = clazz.getSuperclass()) == null) {
                    return null;
                }
                continue;
            }
        }
    }

    static void invokeMethod(final Object obj, final String methodname, final Object... args) {
        Class<?> clazz = obj.getClass();
        do {
            try {
                for (final Method method : clazz.getDeclaredMethods()) {
                    if (method.getName().equals(methodname) && method.getParameterTypes().length == args.length) {
                        method.setAccessible(true);
                        method.invoke(obj, args);
                    }
                }
            }
            catch (Throwable t) {}
        } while ((clazz = clazz.getSuperclass()) != null);
    }

}
