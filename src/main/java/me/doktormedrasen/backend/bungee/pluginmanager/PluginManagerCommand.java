package me.doktormedrasen.backend.bungee.pluginmanager;

import me.doktormedrasen.backend.bungee.Backend;
import me.doktormedrasen.backend.bungee.api.BackendCommand;
import me.doktormedrasen.backend.bungee.api.BackendSender;
import me.doktormedrasen.backend.bungee.api.message.EnglishMessages;
import me.doktormedrasen.backend.bungee.api.message.GermanMessages;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginDescription;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Class by DoktorMedRasen
 */

public class PluginManagerCommand extends BackendCommand {

    public PluginManagerCommand() {
        super("bpm");
    }

    @Override
    public void execute(BackendSender sender, String[] args) {
        if (!sender.hasPermission("system.pluginmanager")) {
            sender.sendMessage(
                    GermanMessages.NOPERMISSIONS,
                    EnglishMessages.NOPERMISSIONS
            );
            return;
        }

        if (args.length == 2 && args[0].equalsIgnoreCase("load")) {
            final Plugin plugin = findPlugin(args[1]);

            if (plugin != null) {
                sender.sendMessage(
                        Backend.PREFIX + "Plugin ist §cbereits §7geladen",
                        Backend.PREFIX + "Plugin is §calready §7loaded"
                );
                return;
            }

            final File file = findFile(args[1]);
            if (!file.exists()) {
                sender.sendMessage(
                        Backend.PREFIX + "Plugin konnte §cnicht §7gefunden werden",
                        Backend.PREFIX + "Plugin could §cnot §7be found"
                );
                return;
            }

            sender.sendMessage(
                    Backend.PREFIX + "§aLade §7Plugin§8...",
                    Backend.PREFIX + "§aLoading §7plugin§8..."
            );
            ProxyServer.getInstance().getScheduler().runAsync(Backend.getInstance(), () -> {
                final boolean success = PluginUtils.loadPlugin(file);
                if (success) {
                    sender.sendMessage(
                            Backend.PREFIX + "Plugin erfolgreich §ageladen",
                            Backend.PREFIX + "Plugin succssfully §aloaded"
                    );
                } else {
                    sender.sendMessage(
                            Backend.PREFIX + "Plugin konnte §cnicht §7geladen werden",
                            Backend.PREFIX + "Plugin could §cnot §7be loaded"
                    );
                }
            });
            return;
        }

        if (args.length == 2 && args[0].equalsIgnoreCase("unload")) {
            if (args[1].equalsIgnoreCase("BungeeBackend")) {
                sender.sendMessage(
                        Backend.PREFIX + "§cBackend §7kann nicht §centladen §7werden",
                        Backend.PREFIX + "§cBackend §7cannot be §cunloaded"
                );
                return;
            }

            final Plugin plugin = findPlugin(args[1]);
            if (plugin == null) {
                sender.sendMessage(
                        Backend.PREFIX + "Plugin konnte §cnicht §7gefunden werden",
                        Backend.PREFIX + "Plugin could §cnot §7be found"
                );
                return;
            }
            sender.sendMessage(
                    Backend.PREFIX + "§cEntlade §7Plugin§8...",
                    Backend.PREFIX + "§cUnloading §7plugin§8..."
            );
            ProxyServer.getInstance().getScheduler().runAsync(Backend.getInstance(), () -> {
               PluginUtils.unloadPlugin(plugin);
               sender.sendMessage(
                       Backend.PREFIX + "Plugin erfolgreich §centladen",
                       Backend.PREFIX + "Plugin successfully §cunloaded"
               );
            });
            return;
        }

        if (args.length == 2 && args[0].equalsIgnoreCase("reload")) {
            if (args[1].equalsIgnoreCase("BungeeBackend")) {
                sender.sendMessage(
                        Backend.PREFIX + "§cBackend §7kann nicht §cneu geladen §7werden",
                        Backend.PREFIX + "§cBackend §7cannot be §creloaded"
                );
                return;
            }

            final Plugin plugin = findPlugin(args[1]);
            if (plugin == null) {
                sender.sendMessage(
                        Backend.PREFIX + "Plugin konnte §cnicht §7gefunden werden",
                        Backend.PREFIX + "Plugin could §cnot §7be found"
                );
                return;
            }

            sender.sendMessage(
                    Backend.PREFIX + "Plugin §8'§f" + plugin.getDescription().getName() + "§8' §7wird §eneu geladen§8...",
                    Backend.PREFIX + "§eReloading plugin §8'§f" + plugin.getDescription().getName() + "§8'§8..."
            );
            ProxyServer.getInstance().getScheduler().runAsync(Backend.getInstance(), () -> {
               File pluginFile = plugin.getFile();
               PluginUtils.unloadPlugin(plugin);
               boolean success = PluginUtils.loadPlugin(pluginFile);
               if (success) {
                   sender.sendMessage(
                           Backend.PREFIX + "Plugin erfolgreich §eneu geladen",
                           Backend.PREFIX + "Plugin successfully §ereloaded"
                   );
               } else {
                   sender.sendMessage(
                           Backend.PREFIX + "Plugin konnte §cnicht §7neu geladen werden",
                           Backend.PREFIX + "Plugin could §cnot §7be reloaded"
                   );
               }
            });
            return;
        }

        sender.sendMessage(
                Backend.PREFIX + "§cPluginManager Befehle §f| §cVerwendung",
                Backend.PREFIX + "§cPluginManager Commands §f| §cUsage"
        );
        sender.sendMessage(Backend.PREFIX);
        sender.sendMessage(Backend.PREFIX + "§f/§cbpm load §f<§cPlugin§f>");
        sender.sendMessage(Backend.PREFIX + "§f/§cbpm unload §f<§cPlugin§f>");
        sender.sendMessage(Backend.PREFIX + "§f/§cbpm reload §f<§cPlugin§f>");
    }

    @Override
    public Iterable<String> onTabComplete(BackendSender sender, String[] args) {
        if (!sender.hasPermission("system.pluginmanager")) {
            return null;
        }
        final List<String> matches = new ArrayList<>();
        if (args.length == 1) {
            final String search = args[0].toLowerCase();
            for (final String string : new String[] {"load", "unload", "reload"}) {
                if (string.startsWith(search)) {
                    matches.add(string);
                }
            }
        } else if (args.length == 2 && args[0].equalsIgnoreCase("load")) {
            String search = args[1].toLowerCase();
            File folder = ProxyServer.getInstance().getPluginsFolder();
            if (folder.exists()) {
                for (File file : folder.listFiles()) {
                    if (file.isFile() && file.getName().endsWith(".jar")) {
                        try {
                            JarFile jar = new JarFile(file);
                            JarEntry pdf = jar.getJarEntry("bungee.yml");
                            if (pdf == null) {
                                pdf = jar.getJarEntry("plugin.yml");
                            }
                            InputStream inputStream = jar.getInputStream(pdf);
                            PluginDescription description = new Yaml().loadAs(inputStream, PluginDescription.class);
                            if (findPlugin(description.getName()) == null && description.getName().toLowerCase().startsWith(search)) {
                                matches.add(description.getName());
                            }
                        } catch (IOException ex) {}
                    }
                }
            }
        } else if (args.length == 2 && (args[0].equalsIgnoreCase("unload") || args[0].equalsIgnoreCase("reload"))) {
            String search = args[1].toLowerCase();
            for (Plugin plugin : ProxyServer.getInstance().getPluginManager().getPlugins()) {
                if (plugin.getDescription().getName().equals("BungeeBackend")) {
                    continue;
                }
                if (!plugin.getDescription().getName().toLowerCase().startsWith(search)) {
                    continue;
                }
                matches.add(plugin.getDescription().getName());
            }
        }
        return matches;
    }

    private static Plugin findPlugin(final String pluginname) {
        for (final Plugin plugin : ProxyServer.getInstance().getPluginManager().getPlugins()) {
            if (plugin.getDescription().getName().equalsIgnoreCase(pluginname)) {
                return plugin;
            }
        }
        return null;
    }

    private static File findFile(final String pluginname) {
        final File folder = ProxyServer.getInstance().getPluginsFolder();
        if (folder.exists()) {
            for (final File file : folder.listFiles()) {
                if (file.isFile() && file.getName().endsWith(".jar")) {
                    try (final JarFile jar = new JarFile(file)) {
                        JarEntry pdf = jar.getJarEntry("bungee.yml");
                        if (pdf == null) {
                            pdf = jar.getJarEntry("plugin.yml");
                        }
                        try (final InputStream in = jar.getInputStream(pdf)) {
                            final PluginDescription desc = new Yaml().loadAs(in, PluginDescription.class);
                            if (desc.getName().equalsIgnoreCase(pluginname)) {
                                return file;
                            }
                        }
                    }
                    catch (Throwable t7) {}
                }
            }
        }
        return new File(folder, pluginname + ".jar");
    }

}